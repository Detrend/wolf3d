using System;
using GLFW;
using static OpenGL.GL;

namespace Wolf
{
    // Graphics submodule of the Wolf3D engine
    public class GraphicsSystem
    {
        // Initializes the graphics module
        public void Init()
        {
            InitWindow();
            InitOpenGl();

            mRenderer3D = new Renderer();
            FramebufferSizeCallback(mWindow, InitialWindowWidth, InitialWindowHeight);
        }

        // Shuts down the graphics module
        public void ShutDown()
        {
            Glfw.Terminate();
            mRenderer3D = null;
        }

        // Returns the renderer
        public Renderer GetRenderer()
        {
            return mRenderer3D;
        }

        // Returns the game window
        public Window GetWindow()
        {
            return mWindow;
        }

        // Updates the graphics.
        // This renders the game, changes window settings and swaps the front and back buffers
        public void Update(float s, SceneSystem state)
        {
            mRenderer3D.Render(state);
            Glfw.SwapBuffers(mWindow);
            Glfw.SetWindowTitle(mWindow, "Wolf3D, dt: " + (s * 1000.0f) + "ms");
        }

        Window   mWindow;
        Renderer mRenderer3D;

        int InitialWindowWidth  = 1280;
        int InitialWindowHeight = 720;

        bool DoubleBuffering = false;

        SizeCallback FramebufferSizeCallbackDel;

        public GraphicsSystem()
        {
            // Garbage collector wants to eat 'FramebufferSizeCallback' function. This is a way to prevent it
            FramebufferSizeCallbackDel = FramebufferSizeCallback;  
        }

        // Callback for the change of size of the application window
        void FramebufferSizeCallback(Window aWindow, int aWidth, int aHeight)
        {
            int MenuHeight = (int)(aHeight * 0.2f);
            mRenderer3D.RecalculateFrustum((float)aWidth / (aHeight - MenuHeight));
            mRenderer3D.WindowWidth = aWidth;
            mRenderer3D.WindowHeight = aHeight;
        }

        // Initializes the game window
        void InitWindow()
        {
            Glfw.WindowHint(Hint.ClientApi, ClientApi.OpenGL);
            Glfw.WindowHint(Hint.ContextVersionMajor, 3);
            Glfw.WindowHint(Hint.ContextVersionMinor, 3);
            Glfw.WindowHint(Hint.OpenglProfile, Profile.Core);
            Glfw.WindowHint(Hint.Doublebuffer, DoubleBuffering);
            Glfw.WindowHint(Hint.Decorated, true);

            mWindow = Glfw.CreateWindow(InitialWindowWidth, InitialWindowHeight, "Wolf3D", Monitor.None, Window.None);

            Glfw.MakeContextCurrent(mWindow);
            Glfw.SetFramebufferSizeCallback(mWindow, FramebufferSizeCallbackDel);
        }

        // Initializes OpenGL rendering API
        void InitOpenGl()
        {
            Import(Glfw.GetProcAddress);
        }
    }

    // Graphics part of the engine
    public sealed partial class Engine
    {
        // Initializes the graphics part of the engine
        public void InitGraphics()
        {
            mGraphics = new GraphicsSystem();
            mGraphics.Init();

            Geometry.GenerateGeometry();
        }

        // Shuts down the engine graphics
        public void ShutDownGraphics()
        {
            Geometry.CleanUp();

            mGraphics.ShutDown();
            mGraphics = null;
        }

        // Returns the engine graphics
        public static GraphicsSystem GetGraphics()
        {
            return GetEngine().mGraphics;
        }

        GraphicsSystem mGraphics;
    }
}

