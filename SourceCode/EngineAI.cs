using System;
using System.Collections.Generic;
using System.Numerics;

namespace Wolf
{
    // // // // // // // //
    // AI engine module  //
    // // // // // // // //
    public sealed partial class Engine
    {
        // Initializes the AI system
        public void InitAiSystem()
        {
            mAI = new AISystem();
            mAI.Init();
        }

        // Shuts down the AI system
        public void ShutDownAiSystem()
        {
            mAI.ShutDown();
            mAI = null;
        }

        // Returns the AI system
        public static AISystem GetAI()
        {
            return Engine.GetEngine().mAI;
        }

        AISystem mAI;
    }

    /////////////
    // AI PLAN //
    /////////////
    public class AIPlan
    {
        public AIPlan(PlanType type, Guard owner, IMap map, List<PlanStep> steps)
        {
            this.planType = type;
            this.owner    = owner;
            this.map      = map;
            this.steps    = steps;
        }

        // Types of a plan that AI can process
        public enum PlanType
        {
            Guard,         // enemy guards a location
            DirectAttack,  // enemy attacks player and runs around him
            FollowPlayer,  // enemy moves towards the position he saw the player last time
            HealYourself,  // enemy moves towards a healing item
            ReturnToSpawn, // enemy returns to the place he spawned at
        }

        public enum StepType
        {
            Attack,     // attack the player
            Move,       // moves towards a position
            PickUp,     // pick up a heal item
            Alert,      // alert surrounding
            Wait,       // wait for some time before doint the next step
        }

        public struct PlanStep
        {
            public StepType type;
            public Path     path;       // path. Used only by move plan
            public float    waitTime;   // time to wait. Used only by wait plan
            public int      attackCount;// how many times to attack. Used only by attack plan
        }

        public PlanType       planType;
        public List<PlanStep> steps;
        public IMap           map;
        public Guard          owner;
    }

    // Type of an AI event that can be streamed from a position
    public enum AIEvent
    {
        Death = 0,
        Alert,
        DoorOpen,
        Shooting,
        ShootingLoud,
        Count
    }

    // // // // // //
    //  AI SYSTEM  //
    // // // // // //
    public class AISystem
    {
        // Loudness of each AIEvent (how long it spreads in meters if behind a wall)
        static readonly float[] EVENT_DISTANCE = new float[(int)AIEvent.Count] 
        {
            5.0f,       // death
            8.0f,       // alert
            4.0f,       // door open
            10.0f,      // shooting
            15.0f,      // shooting loud
        };

        public void Init()
        {
            // do nothing
        }

        public void ShutDown()
        {
            // do nothing
        }

        Random mRandom = new Random();

        // Notifies all enemies in proximity about the event that happened
        // and potentially changes their plans
        public void StreamEvent(AIEvent e, Vector2 position, IMap map)
        {
            float eventDistance = EVENT_DISTANCE[(int)e];

            // notify all enemies in proximity
            foreach (var enemy in map.Enemies)
            {
                var enemyPos = enemy.GetPosition();
                var distance = Vector2.Distance(position, enemyPos);

                bool canSee = enemy.CanSeePosition(position);
                float adjustedEventDistance = canSee ? eventDistance * 10.0f : eventDistance;
                if (distance > adjustedEventDistance)
                    continue;

                if (enemy.mState == State.Dead)
                {
                    continue;
                }

                if (canSee)
                {
                    enemy.FaceTowards(position);
                }
                else
                {
                    var plan = CreateGuardTowardsPlan(enemy, map, position);
                    if (plan != null)
                    {
                        enemy.SetPlan(plan);
                    }
                }
            }
        }

        // ============ Construction of plans ============
        // SCOUT CERTAIN POSITION
        AIPlan CreateGuardTowardsPlan(Guard guard, IMap map, Vector2 position)
        {
            var steps = new List<AIPlan.PlanStep>();
            var step = new AIPlan.PlanStep();
            step.type = AIPlan.StepType.Move;
            step.path = map.CreateFollowPath(guard.GetPosition(), position, 50);
            if (step.path == null)
            {
                return null;
            }
            steps.Add(step);

            return new AIPlan(
                AIPlan.PlanType.Guard,
                guard,
                map,
                steps);
        }

        // GO TOWARDS THE LAST LOCATION THE PLAYER WAS SEEN AT
        AIPlan CreateFollowPlayerPlan(Guard guard, IMap map, AIPlan previous)
        {
            var steps = new List<AIPlan.PlanStep>();
            var step = new AIPlan.PlanStep();
            step.type = AIPlan.StepType.Move;
            step.path = map.CreateFollowPath(guard.GetPosition(), guard.mLastSeenPlayerPos, 50);
            steps.Add(step);

            return new AIPlan(
                AIPlan.PlanType.FollowPlayer,
                guard,
                map,
                steps);
        }

        // ATTACK THE PLAYER DIRECTLY
        AIPlan CreateDirectAttackPlan(Guard guard, IMap map, AIPlan previous)
        {
            var steps = new List<AIPlan.PlanStep>();
            var alert = new AIPlan.PlanStep();
            alert.type = AIPlan.StepType.Alert;
            alert.waitTime = 0.25f;
            var attack1 = new AIPlan.PlanStep();
            attack1.type = AIPlan.StepType.Attack;
            attack1.attackCount = 2;
            var strafe = new AIPlan.PlanStep();
            strafe.type = AIPlan.StepType.Move;
            strafe.path = map.CreateAttackPath(guard.GetPosition(), 3);
            var attack2 = new AIPlan.PlanStep();
            attack2.type = AIPlan.StepType.Attack;
            attack2.attackCount = 3;

            steps.Add(alert);
            steps.Add(attack1);
            steps.Add(strafe);
            steps.Add(attack2);

            return new AIPlan(
                AIPlan.PlanType.DirectAttack,
                guard,
                map,
                steps);
        }

        // GUARD THE AREA
        AIPlan CreateGuardPlan(Guard guard, IMap map, AIPlan previous)
        {
            var steps = new List<AIPlan.PlanStep>();
            var step = new AIPlan.PlanStep();
            step.type = AIPlan.StepType.Wait;
            step.waitTime = 10.0f;
            steps.Add(step);

            return new AIPlan(
                AIPlan.PlanType.Guard,
                guard,
                map,
                steps);
        }

        static readonly int CHANCE_TO_WAIT = 5;

        // RETURN BACK TO SPAWN
        AIPlan CreateReturnBackToSpawnPlan(Guard guard, IMap map, AIPlan previous)
        {
            var steps = new List<AIPlan.PlanStep>();
            if (mRandom.Next(0, 10) < CHANCE_TO_WAIT)
            {
                var wait = new AIPlan.PlanStep();
                wait.waitTime = mRandom.Next(0, 100) * 0.1f;
                steps.Add(wait);
            }
            var step  = new AIPlan.PlanStep();
            step.type = AIPlan.StepType.Move;
            step.path = map.CreateFollowPath(guard.GetPosition(), guard.spawnPosition, 50);
            if (step.path == null)
            {
                return null;
            }
            steps.Add(step);

            return new AIPlan(
                AIPlan.PlanType.ReturnToSpawn,
                guard,
                map,
                steps);
        }

        // HEAL PLAN
        AIPlan CreateHealYourselfPlan(Guard guard, IMap map, AIPlan previous)
        {
            var myPos = guard.GetPosition();

            // find nearest pickable object in the area
            IRenderable pickup = null;
            foreach (var p in map.Pickups)
            {
                var cast = p as IPickup;
                if (!(cast != null && cast.CanUse(guard)))
                {
                    continue;
                }
                var pos = p.GetPosition();
                float distance = Vector2.Distance(myPos, pos);
                if (distance < Guard.MAX_DISTANCE_TO_COLLECT_PICKUP)
                {
                    pickup = p;
                    break;
                }
            }

            if (pickup == null)
            {
                return null;
            }

            var steps = new List<AIPlan.PlanStep>();
            var step = new AIPlan.PlanStep();
            step.type = AIPlan.StepType.Move;
            step.path = map.CreateFollowPath(myPos, pickup.GetPosition(), 50);
            if (step.path == null)
            {
                return null;
            }
            steps.Add(step);

            return new AIPlan(
                AIPlan.PlanType.HealYourself,
                guard,
                map,
                steps);
        }

        // "Thinks" for the enemy and assigns it a new plan
        public AIPlan MakePlan(Guard guard, IMap map, AIPlan previousPlan)
        {
            if (guard.mState == State.Alert)
            {
                // if we can see the player
                if (guard.CanSpotPlayer())
                {
                    return CreateDirectAttackPlan(guard, map, previousPlan);
                }

                if (previousPlan != null && previousPlan.planType == AIPlan.PlanType.DirectAttack)
                {
                    return CreateFollowPlayerPlan(guard, map, previousPlan);
                }

                if (guard.mHP < Guard.GUARD_MAX_HP)
                {
                    var plan = CreateHealYourselfPlan(guard, map, previousPlan);
                    if (plan != null)
                    {
                        return plan;
                    }
                }
                else
                {
                    var plan = CreateReturnBackToSpawnPlan(guard, map, previousPlan);
                    if (plan != null)
                    {
                        return plan;
                    }
                }

                if (previousPlan != null
                && (previousPlan.planType == AIPlan.PlanType.FollowPlayer
                ||  previousPlan.planType == AIPlan.PlanType.HealYourself))
                {
                    var plan = CreateReturnBackToSpawnPlan(guard, map, previousPlan);
                    if (plan != null)
                    {
                        return plan;
                    }
                }

                return CreateFollowPlayerPlan(guard, map, previousPlan);
            }
            else
            {
                if (Vector2.Distance(guard.GetPosition(), guard.spawnPosition) > 3.0f)
                {
                    var plan = CreateReturnBackToSpawnPlan(guard, map, previousPlan);
                    if (plan != null)
                    {
                        return plan;
                    }
                }
                return CreateGuardPlan(guard, map, previousPlan);
            }
        }

        // Checks if the given plan is still valid (or should be discarded)
        public bool IsPlanValid(AIPlan plan)
        {
            if (plan == null)
            {
                return false;
            }

            // invalidate the guard plan if we can see the player
            if (plan.planType == AIPlan.PlanType.Guard)
            {
                if (plan.owner.mState == State.Alert)
                {
                    return false;
                }
            }

            // invalidate the guard plan if we cannot spot the player anymore
            if (plan.planType == AIPlan.PlanType.DirectAttack && !plan.owner.CanSpotPlayer())
            {
                return false;
            }

            // these plans are invalid if we suddenly spot the player
            if (plan.owner.CanSpotPlayer())
            {
                if (plan.planType == AIPlan.PlanType.FollowPlayer)
                    return false;
                if (plan.planType == AIPlan.PlanType.HealYourself)
                    return false;
                if (plan.planType == AIPlan.PlanType.Guard)
                    return false;
                if (plan.planType == AIPlan.PlanType.ReturnToSpawn)
                    return false;
            }

            return true;
        }
    }
}

