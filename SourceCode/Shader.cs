﻿using System;
using System.IO;
using static OpenGL.GL;

namespace Wolf
{
    // Shader helper
    class Shader
    {
        // Compiles a shader from a file and returns its handle
        public static uint CompileFromFile(int aShaderType, string aFilePath)
        {
            uint shaderHandle = glCreateShader(aShaderType);
            string source = File.ReadAllText(aFilePath);

            glShaderSource(shaderHandle, source);
            glCompileShader(shaderHandle);

            string log = glGetShaderInfoLog(shaderHandle);
            if (log.Length > 0)
                Console.WriteLine("[Shader compiler]: " + log);

            return shaderHandle;
        }

        // Links vertex and fragment shaders and creates a GPU program
        // Does not delete shaders afterwards, so clean them up yourself!
        public static uint LinkShaders(uint aVertex, uint aFragment)
        {
            uint programHandle = glCreateProgram();
            glAttachShader(programHandle, aVertex);
            glAttachShader(programHandle, aFragment);

            glLinkProgram(programHandle);
            string log = glGetProgramInfoLog(programHandle);
            if (log.Length > 0)
                Console.WriteLine("[Shader linker]: " + log);

            return programHandle;
        }
    }
}
