using System;
using GLFW;         // window and inputs

namespace Wolf
{
    public sealed class InputSystem
    {
        // Initializes the engine input system
        public void Init(GLFW.Window window)
        {
            mWindow = window;
            mInput = new InputKeyboard();
        }

        // Shuts down the input system
        public void ShutDown()
        {
            mInput = null;
        }

        // Updates the input data structure
        public void Update(float s)
        {
            Glfw.PollEvents();

            mInput.Forward   = (Glfw.GetKey(mWindow, Keys.W)         == InputState.Press);
            mInput.Back      = (Glfw.GetKey(mWindow, Keys.S)         == InputState.Press);
            mInput.Sprinting = (Glfw.GetKey(mWindow, Keys.LeftShift) == InputState.Press);
            mInput.Shooting  = (Glfw.GetKey(mWindow, Keys.LeftControl) == InputState.Press);
            mInput.Equipped0 = (Glfw.GetKey(mWindow, Keys.Alpha1)    == InputState.Press);
            mInput.Equipped1 = (Glfw.GetKey(mWindow, Keys.Alpha2)    == InputState.Press);
            mInput.Equipped2 = (Glfw.GetKey(mWindow, Keys.Alpha3)    == InputState.Press);
            mInput.Equipped3 = (Glfw.GetKey(mWindow, Keys.Alpha4)    == InputState.Press);
            mInput.Using     = (Glfw.GetKey(mWindow, Keys.Space)     == InputState.Press);

            if (Glfw.GetKey(mWindow, Keys.LeftAlt) == InputState.Press)
            {
                mInput.RotLeft  = false;
                mInput.RotRight = false;
                mInput.Left  = (Glfw.GetKey(mWindow, Keys.A) == InputState.Press);
                mInput.Right = (Glfw.GetKey(mWindow, Keys.D) == InputState.Press);
            }
            else
            {
                mInput.RotLeft  = (Glfw.GetKey(mWindow, Keys.A) == InputState.Press);
                mInput.RotRight = (Glfw.GetKey(mWindow, Keys.D) == InputState.Press);
                mInput.Left  = false;
                mInput.Right = false;
            }
        }

        // Returns inputs for this frame
        public IInput GetInput()
        {
            return mInput;
        }

        GLFW.Window   mWindow;
        InputKeyboard mInput;
    }

    // Part of the engine that handles inputs
    public sealed partial class Engine
    {
        // Initializes the input system
        // Call this only after initializing the window
        void InitInputSystem(GLFW.Window aWindow)
        {
            mInputSystem = new InputSystem();
            mInputSystem.Init(aWindow);
        }

        // Shuts down the input system
        void ShutDownInputSystem()
        {
            mInputSystem.ShutDown();
            mInputSystem = null;
        }

        // Returns the reference to the input system
        // Engine.GetInputSystem
        public static InputSystem GetInputSystem()
        {
            return mEngineSingleton.mInputSystem;
        }

        InputSystem mInputSystem;
    }
}

