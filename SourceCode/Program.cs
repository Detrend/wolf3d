﻿using System;

namespace Wolf
{
    class Program
    {
        // Runs the engine, waits until it finished and then cleans it up
        static void Main(string[] args)
        {
            Engine Wolf = new Engine();

            try
            {
                Wolf.Init();
                Wolf.Run();
                Wolf.Exit();
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Wolf3D was crashed by an exception. Reason: {0}", ex.Message);
                return;
            }
        }
    }
}