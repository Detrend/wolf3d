﻿using System;
using System.Numerics;
using static OpenGL.GL;

namespace Wolf
{
    // Renderer. Renders walls, billboards, background, HUD...
    public class Renderer
    {
        private Matrix4x4 mFrustum;
        private uint mGpuProgramWalls;
        private uint mGpuProgramBillboards;
        private uint mGpuProgramBackground;
        private uint mGpuProgramGun;
        private uint mGpuProgramHotbar;
        private uint mGpuProgramLoading;

        // Horizontal FOV degrees
        private const float mFovDegrees = 50.0f;
        private float       mAspect;    // aspect ratio of the screen

        // Renders guards through walls. Only for debugging
        static readonly bool SHOW_GUARDS_THROUGH_WALLS_DEBUG = false;

        // Size of the window
        public int WindowWidth;
        public int WindowHeight;

        static readonly string WALL_SHADER_VER  = "Shaders/walls.vert";
        static readonly string WALL_SHADER_FRAG = "Shaders/walls.frag";

        static readonly string BILLBOARD_SHADER_VER  = "Shaders/billboards.vert";
        static readonly string BILLBOARD_SHADER_FRAG = "Shaders/billboards.frag";

        static readonly string BACK_SHADER_VER  = "Shaders/background.vert";
        static readonly string BACK_SHADER_FRAG = "Shaders/background.frag";

        static readonly string GUN_SHADER_VER  = "Shaders/gun.vert";
        static readonly string GUN_SHADER_FRAG = "Shaders/gun.frag";

        static readonly string HOTBAR_SHADER_VER  = "Shaders/hotbar.vert";
        static readonly string HOTBAR_SHADER_FRAG = "Shaders/hotbar.frag";

        static readonly string LOADING_SHADER_VER  = "Shaders/loading.vert";
        static readonly string LOADING_SHADER_FRAG = "Shaders/loading.frag";

        public Renderer()
        {
            RecalculateFrustum(16.0f / 9.0f);

            uint shaderVertexWalls   = Shader.CompileFromFile(GL_VERTEX_SHADER, Engine.RESOURCES_DIR   + WALL_SHADER_VER);
            uint shaderFragmentWalls = Shader.CompileFromFile(GL_FRAGMENT_SHADER, Engine.RESOURCES_DIR + WALL_SHADER_FRAG);
            mGpuProgramWalls = Shader.LinkShaders(shaderVertexWalls, shaderFragmentWalls);

            uint shaderVertexBill   = Shader.CompileFromFile(GL_VERTEX_SHADER, Engine.RESOURCES_DIR   + BILLBOARD_SHADER_VER);
            uint shaderFragmentBill = Shader.CompileFromFile(GL_FRAGMENT_SHADER, Engine.RESOURCES_DIR + BILLBOARD_SHADER_FRAG);
            mGpuProgramBillboards = Shader.LinkShaders(shaderVertexBill, shaderFragmentBill);

            uint shaderVertexBack   = Shader.CompileFromFile(GL_VERTEX_SHADER, Engine.RESOURCES_DIR   + BACK_SHADER_VER);
            uint shaderFragmentBack = Shader.CompileFromFile(GL_FRAGMENT_SHADER, Engine.RESOURCES_DIR + BACK_SHADER_FRAG);
            mGpuProgramBackground = Shader.LinkShaders(shaderVertexBack, shaderFragmentBack);

            uint shaderVertexGun   = Shader.CompileFromFile(GL_VERTEX_SHADER, Engine.RESOURCES_DIR   + GUN_SHADER_VER);
            uint shaderFragmentGun = Shader.CompileFromFile(GL_FRAGMENT_SHADER, Engine.RESOURCES_DIR + GUN_SHADER_FRAG);
            mGpuProgramGun = Shader.LinkShaders(shaderVertexGun, shaderFragmentGun);

            uint shaderVertexHotbar = Shader.CompileFromFile(GL_VERTEX_SHADER, Engine.RESOURCES_DIR     + HOTBAR_SHADER_VER);
            uint shaderFragmentHotbar = Shader.CompileFromFile(GL_FRAGMENT_SHADER, Engine.RESOURCES_DIR + HOTBAR_SHADER_FRAG);
            mGpuProgramHotbar = Shader.LinkShaders(shaderVertexHotbar, shaderFragmentHotbar);

            uint shaderVertexLoading = Shader.CompileFromFile(GL_VERTEX_SHADER, Engine.RESOURCES_DIR     + LOADING_SHADER_VER);
            uint shaderFragmentLoading = Shader.CompileFromFile(GL_FRAGMENT_SHADER, Engine.RESOURCES_DIR + LOADING_SHADER_FRAG);
            mGpuProgramLoading = Shader.LinkShaders(shaderVertexLoading, shaderFragmentLoading);

            glDeleteShader(shaderVertexWalls);
            glDeleteShader(shaderFragmentWalls);

            glDeleteShader(shaderVertexLoading);
            glDeleteShader(shaderFragmentLoading);

            glDeleteShader(shaderVertexBill);
            glDeleteShader(shaderFragmentBill);

            glDeleteShader(shaderVertexBack);
            glDeleteShader(shaderFragmentBack);

            glDeleteShader(shaderVertexGun);
            glDeleteShader(shaderFragmentGun);

            glDeleteShader(shaderVertexHotbar);
            glDeleteShader(shaderFragmentHotbar);
        }

        // Recalculates the view frustum on screen aspect change
        public void RecalculateFrustum(float aAspect)
        {
            mAspect = aAspect;
            mFrustum = Matrix4x4.CreatePerspectiveFieldOfView((mFovDegrees / 180.0f) * (float)Math.PI, aAspect, 0.01f, 1000.0f);
        }

        ~Renderer()
        {
            glDeleteProgram(mGpuProgramWalls);
            glDeleteProgram(mGpuProgramBillboards);
            glDeleteProgram(mGpuProgramBackground);
        }

        // Checks if a given object is visible on the screen
        bool IsVisible(Vector2 aCoord, IMap aMap, float aMaxDot)
        {
            Vector2 PlayerFacing = aMap.Player.GetFacing();
            Vector2 PlayerPosition = aMap.Player.GetPosition();
            Vector2 PlayerToPoint = aCoord - PlayerPosition;

            bool Angle = Vector2.Dot(Vector2.Normalize(PlayerFacing), Vector2.Normalize(PlayerToPoint)) > aMaxDot;

            return (Angle);
        }

        // Renders the top and bottom parts of the background (ceiling and floor)
        void RenderBackground(Vector3 aFloorColor, Vector3 aCeilingColor, SceneSystem aState)
        {
            var playerData = Engine.GetScene().GetPlayerData();

            glUseProgram(mGpuProgramBackground);
            glUniform3f(0, aFloorColor.X, aFloorColor.Y, aFloorColor.Z);
            glUniform3f(1, aCeilingColor.X, aCeilingColor.Y, aCeilingColor.Z);
            glUniform4f(2, playerData.HurtGlow, playerData.ScoreGlow, 0.0f, 0.0f);
            glDrawArrays(GL_TRIANGLES, 0, 6);
        }

        // Renders the walls
        void RenderWalls(SceneSystem aState, Matrix4x4 aWorldToCamera)
        {
            var playerData = Engine.GetScene().GetPlayerData();

            glUseProgram(mGpuProgramWalls);
            glUniform1i(4, 0);  // texture unit 0 = shader binding 4
            glUniform1i(5, 1);
            glUniform1i(6, 2);
            glUniform1i(7, 3);
            glUniform4f(8, playerData.HurtGlow, playerData.ScoreGlow, 0.0f, 0.0f);

            IMap aMap = aState.GetMap();
            int Count = aMap.Walls.Count + aMap.Doors.Count;

            for (int i = 0; i < Count; i++)
            {
                glBindVertexArray(Geometry.WallVertexArrayHandle);

                IRenderable wall;
                if (i < aMap.Walls.Count)
                {
                    wall = aMap.Walls[i];

                    uint[] Textures = wall.GetTextures();
                    if (Textures.Length != 4)
                        continue;

                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, Textures[0]);
                    glActiveTexture(GL_TEXTURE0 + 1);
                    glBindTexture(GL_TEXTURE_2D, Textures[1]);
                    glActiveTexture(GL_TEXTURE0 + 2);
                    glBindTexture(GL_TEXTURE_2D, Textures[2]);
                    glActiveTexture(GL_TEXTURE0 + 3);
                    glBindTexture(GL_TEXTURE_2D, Textures[3]);
                }
                else
                {
                    wall = aMap.Doors[i - aMap.Walls.Count];

                    uint[] Textures = wall.GetTextures();
                    if (Textures.Length != 1)
                        continue;

                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, Textures[0]);
                }

                Vector3 Position = new Vector3(wall.GetPosition().X, 0.0f, wall.GetPosition().Y);
                Matrix4x4 LocalToWorld = Matrix4x4.CreateTranslation(Position);

                float MaxDot = 0.5f;
                if (!IsVisible(wall.GetPosition(), aMap, MaxDot) 
                    && !IsVisible(wall.GetPosition() + new Vector2(0.9999f, 0.0f), aMap, MaxDot) 
                    && !IsVisible(wall.GetPosition() + new Vector2(0.0f, 0.9999f), aMap, MaxDot)
                    && !IsVisible(wall.GetPosition() + new Vector2(0.9999f, 0.9999f), aMap, MaxDot))
                {
                    continue;
                }


                uint VertexBuffer = wall.GetVB();
                uint IndexBuffer = wall.GetIB();

                glUniformMatrix4fv(0, 1, false, Math3D.MatToFloats(mFrustum));
                glUniformMatrix4fv(1, 1, false, Math3D.MatToFloats(aWorldToCamera));
                glUniformMatrix4fv(2, 1, false, Math3D.MatToFloats(LocalToWorld));

                glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBuffer);

                unsafe
                {
                    glVertexAttribPointer(0, 3, GL_FLOAT, false, 6 * sizeof(float), NULL);
                    glEnableVertexAttribArray(0);
                    glVertexAttribPointer(1, 2, GL_FLOAT, false, 6 * sizeof(float), (void*)(3 * sizeof(float)));
                    glEnableVertexAttribArray(1);
                    glVertexAttribPointer(2, 1, GL_FLOAT, false, 6 * sizeof(float), (void*)(5 * sizeof(float)));
                    glEnableVertexAttribArray(2);

                    glDrawElements(GL_TRIANGLES, wall.GetIndicesCount(), GL_UNSIGNED_INT, null);
                }
            }

            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        // Renders billboards (enemies, items, ...)
        void RenderBillboards(SceneSystem aState, Matrix4x4 aWorldToCamera)
        {
            var playerData = Engine.GetScene().GetPlayerData();

            glUseProgram(mGpuProgramBillboards);
            glUniform1i(4, 0);  // texture unit 0 = shader binding 4
            glUniform4f(5, playerData.HurtGlow, playerData.ScoreGlow, 0.0f, 0.0f);
            glUniform4i(6, 1, 1, 0, 0);

            IMap aMap = aState.GetMap();
            int BillboardsCount = aMap.Billboards.Count;
            int PickupsCount = aMap.Pickups.Count;
            int EnemiesCount = aMap.Enemies.Count;

            for (int i = 0; i < BillboardsCount + PickupsCount + EnemiesCount; i++)
            {
                bool isEnemy = false;

                IRenderable bill;
                if (i < BillboardsCount)
                {
                    bill = aMap.Billboards[i];
                }
                else if (i < BillboardsCount + PickupsCount)
                {
                    bill = aMap.Pickups[i - BillboardsCount];
                }
                else
                {
                    isEnemy = true;
                    bill = aMap.Enemies[i - (BillboardsCount + PickupsCount)];
                    Enemy en = (Enemy)bill;
                    int[] Indices = en.GetSheetIndices();
                    if (Indices.Length != 4)
                        continue;
                    glUniform4i(6, Indices[0], Indices[1], Indices[2], Indices[3]);
                }

                Vector3 Position = new Vector3(bill.GetPosition().X, 0.0f, bill.GetPosition().Y);
                Matrix4x4 LocalToWorld = Matrix4x4.CreateTranslation(Position);

                float MaxDot = 0.5f;
                if (!IsVisible(bill.GetPosition(), aMap, MaxDot)
                    && !IsVisible(bill.GetPosition() + new Vector2(1.0f, 0.0f), aMap, MaxDot)
                    && !IsVisible(bill.GetPosition() + new Vector2(0.0f, 1.0f), aMap, MaxDot)
                    && !IsVisible(bill.GetPosition() + new Vector2(1.0f, 1.0f), aMap, MaxDot))
                {
                    continue;
                }

                uint VertexBuffer = bill.GetVB();
                uint IndexBuffer = bill.GetIB();

                glUniformMatrix4fv(0, 1, false, Math3D.MatToFloats(mFrustum));
                glUniformMatrix4fv(1, 1, false, Math3D.MatToFloats(aWorldToCamera));
                glUniformMatrix4fv(2, 1, false, Math3D.MatToFloats(LocalToWorld));

                uint[] Textures = bill.GetTextures();
                if (Textures.Length != 1)
                    continue;

                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, Textures[0]);

                glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBuffer);

                if (isEnemy && SHOW_GUARDS_THROUGH_WALLS_DEBUG)
                {
                    glDisable(GL_DEPTH_TEST);
                }

                unsafe
                {
                    glDrawElements(GL_TRIANGLES, bill.GetIndicesCount(), GL_UNSIGNED_INT, null);
                }

                if (isEnemy && SHOW_GUARDS_THROUGH_WALLS_DEBUG)
                {
                    glEnable(GL_DEPTH_TEST);
                }
            }
        }

        // Renders the gun in player's hands
        void RenderGun(IMap aMap, int aGun, int aGunIndex)
        {
            glUseProgram(mGpuProgramGun);
            glUniform1f(0, mAspect);        // screen aspect ratio
            glUniform1i(1, 0);              // texture unit 0 = shader binding 1
            glUniform1i(2, aGunIndex);      // sprite index of a gun
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, aMap.GunTextures[aGun]);
            glDrawArrays(GL_TRIANGLES, 0, 6);
        }

        // Renders the hotbar with ammo count, HP count, score and the character face
        void RenderHotbar(SceneSystem aState)
        {
            var playerData = Engine.GetScene().GetPlayerData();

            glUseProgram(mGpuProgramHotbar);
            glUniform1i(0, 0);          //  hud texture
            glUniform1i(1, 1);          //  face texture
            glUniform1i(2, 2);          //  number font texture
            glUniform1i(3, 3);          //  gun icons
            glUniform4i(4, (int)playerData.FaceIndex, playerData.Ammo, playerData.Score, playerData.Floor);
            glUniform4i(5, playerData.Lives, playerData.HP, playerData.CurrentGun, 0);

            for (int i = 0; i < 4; i++)
            {
                glActiveTexture(GL_TEXTURE0 + i);
                glBindTexture(GL_TEXTURE_2D, aState.GetMap().HotbarTextures[i]);
            }     

            glDrawArrays(GL_TRIANGLES, 0, 6);
        }

        // Renders the loading screen of the level
        public void RenderLoadingScreen(uint aImage)
        {
            glViewport(0, 0, WindowWidth, WindowHeight);
            glClearColor(0.0f, 64.0f / 255.0f, 64.0f / 255.0f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);
            glDisable(GL_DEPTH_TEST);
            glUseProgram(mGpuProgramLoading);
            glUniform1i(0, 0);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, aImage);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            glFlush();
        }

        // Renders the whole level
        public void Render(SceneSystem aState)
        {
            IMap map = aState?.GetMap();

            if (map == null || map.Player == null)
                return;

            int MenuHeight = (int)(WindowHeight * 0.2f);
            glViewport(0, MenuHeight, WindowWidth, WindowHeight - MenuHeight);

            glFrontFace(GL_CCW);
            glEnable(GL_CULL_FACE);

            glClearColor(0.0f, 64.0f / 255.0f, 64.0f / 255.0f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);
            glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            glClear(GL_DEPTH_BUFFER_BIT);
            glDisable(GL_DEPTH_TEST);

            RenderBackground(map.FloorColor, map.CeilingColor, aState);

            Vector3 PlayerPosition = new Vector3(map.Player.GetPosition().X, 0.5f, map.Player.GetPosition().Y);
            Vector3 PlayerFacing = PlayerPosition + new Vector3(map.Player.GetFacing().X, 0.0f, map.Player.GetFacing().Y);
            Vector3 Up = new Vector3(0.0f, 1.0f, 0.0f);
            Matrix4x4 WorldToCamera = Matrix4x4.CreateLookAt(PlayerPosition, PlayerFacing, Up);

            var playerData = Engine.GetScene().GetPlayerData();

            glEnable(GL_DEPTH_TEST);
            RenderWalls(aState, WorldToCamera);

            glBindVertexArray(Geometry.BillboardVertexArrayHandle);
            RenderBillboards(aState, WorldToCamera);
            glDisable(GL_DEPTH_TEST);
            RenderGun(map, playerData.CurrentGun, (int)playerData.GunIndex);

            glViewport(0, 0, WindowWidth, MenuHeight);
            RenderHotbar(aState);

            glFlush();
        }
    }
}
