﻿using System.Numerics;

namespace Wolf
{
    class Math3D
    {
        // Converts a matrix to array of floats so we can fetch it to OpenGL
        static public float[] MatToFloats(Matrix4x4 aMatrix)
        {
            return new float[] { aMatrix.M11, aMatrix.M12, aMatrix.M13, aMatrix.M14,
                                 aMatrix.M21, aMatrix.M22, aMatrix.M23, aMatrix.M24,
                                 aMatrix.M31, aMatrix.M32, aMatrix.M33, aMatrix.M34,
                                 aMatrix.M41, aMatrix.M42, aMatrix.M43, aMatrix.M44 };
        }
    }
}
