﻿namespace Wolf
{
    // Readable chunk of data that represents current player's stats
    public class PlayerData
    {
        public int          HP;
        public int          Score;
        public int          Ammo;
        public int          CurrentGun;
        public int          Floor;
        public int          Lives;
        public bool[]       HasGun;
        public bool         CanShoot;
        public float[]      GunShootingSpeed;
        public bool[]       GunRepeatedShooting;
        public int[]        GunDamage;
        public int[]        GunAmmoConsumption;
        public SoundType[]  GunSounds;
        public float[]      GunRange;
        public float        GunIndex;
        public float        FaceIndex;
        public float        HurtGlow;
        public float        ScoreGlow;

        public PlayerData(int aHP, int aScore, int aAmmo, int aCurrentGun)
        {
            HP      = aHP;
            Score   = aScore;
            Ammo                = aAmmo;
            CurrentGun          = aCurrentGun;
            CanShoot            = true;
            HasGun              = new bool[] { true, true, false, false };
            GunShootingSpeed    = new float[] { 0.3f, 0.3f, 0.4f, 0.45f };
            GunRepeatedShooting = new bool[] { false, false, true, true };
            GunSounds           = new SoundType[] { SoundType.Knife, SoundType.Pistol, SoundType.Smg, SoundType.Machinegun};
            GunAmmoConsumption  = new int[] { 0, 1, 1, 1 };
            GunDamage           = new int[] { 35, 50, 60, 80 };
            GunRange            = new float[] { 1.0f, 100.0f, 100.0f, 100.0f };
            GunIndex    = 0;
            FaceIndex   = 0.0f;
            Lives       = 3;
            Floor       = 1;
            HurtGlow    = 0.0f;
            ScoreGlow   = 0.0f;
        }
    }
}
