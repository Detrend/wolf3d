using System;
using System.Collections.Generic;
using System.IO;

namespace Wolf
{
    // A scene part of the engine.
    // Scene system takes care of the scene and the map
    public sealed partial class Engine
    {
        // Updates the whole scene (player movement, enemies, enemyAI, moving objects...)
        public void UpdateSceneSystem(float s)
        {
            if (mSceneSystem.GetMap().Finished == true) // load the next map if the current one was already finished
            {
                if (mMapList.Count == 0)
                {
                    throw new Exception("Run out of maps");
                }

                var OldMap = mSceneSystem.GetMap();
                var NewMap = new MapPng(mMapList[0]);
                mMapList.RemoveAt(0);

                NewMap.LoadMap();
                mSceneSystem.SetMap(NewMap);
                OldMap.Cleanup();       // clear all resources allocated by the old map
            }

            // Updates the scene
            mSceneSystem.Update(s);
        }

        // Shuts down the scene system
        public void ShutDownSceneSystem()
        {
            mSceneSystem.ShutDown();
            mSceneSystem = null;
        }

        // Returns the scene system
        public static SceneSystem GetScene()
        {
            return Engine.GetEngine().mSceneSystem;
        }

        // Initializes the scene system
        public void InitSceneSystem()
        {
            mSceneSystem = new SceneSystem();
            mSceneSystem.Init();

            mMapList = new List<string>();

            LoadMapList(RESOURCES_DIR + MAP_FILE_NAME);
            LoadMapFromFile(mMapList[0]);
            mMapList.RemoveAt(0);
        }

        // ============ PRIVATE ============ //

        // This file contains list of all maps and their order.
        // Each line of file contains a path to one map on the disk
        static readonly string MAP_FILE_NAME = "maps.txt";  

        // Loads a map directly from a file given by the path
        void LoadMapFromFile(string aMapName)
        {
            GetScene().SetMap(aMapName);
            uint LoadingScreen = GetScene().GetMapLoadingScreen();
            GetGraphics().GetRenderer().RenderLoadingScreen(LoadingScreen);
            GetScene().LoadMap();
        }

        // Loads list of maps that should be playable in the game
        // After finishing one map the one following it in the list is loaded.
        void LoadMapList(string aListPath)
        {
            foreach (string MapName in File.ReadLines(aListPath))
            {
                mMapList.Add(MapName);
            }
        }

        // ======= MEMBERS ======== //
        SceneSystem  mSceneSystem;  // pointer to the scene system
        List<string> mMapList;      // list of all maps that should be played
    }

    // // // // // // //
    //  SCENE SYSTEM  //
    // // // // // // //
    public class SceneSystem
    {
        // Initializes the scene system
        public void Init()
        {
            // (do nothing)
        }

        // Shuts down the scene system
        public void ShutDown()
        {
            mCurrentMap.Cleanup();
        }

        // Returns the players data
        public PlayerData GetPlayerData()
        {
            return mCurrentMap?.Player?.GetData();
        }

        // Updates the scene system
        public void Update(float s)
        {
            if (mCurrentMap == null || !mCurrentMap.mLoaded)
                return;

            long millis = (long)(s * 1000.0f);

            AdvanceActive(millis);  // updates active objects (guards, player, moving walls, ...)
            AdvancePickups();       // checks if objects laying on the ground can be picked up
        }

        // Returns the currently played map
        public IMap GetMap()
        {
            return mCurrentMap;
        }

        // Loads a map from a file name and replaces the current map by it
        public void SetMap(string aMapName)
        {
            var newMap = new MapPng(aMapName);
            SetMap(newMap);
        }

        // Replaces the current map by a new one.
        // The new map must be loaded first before playing it
        public void SetMap(IMap aMap)
        {
            mCurrentMap = aMap;
        }

        // Loads resources of the given map
        public void LoadMap()
        {
            mCurrentMap.LoadMap();
        }

        // Returns a handle to the loading screen of the current map.
        // The loading screen should be displayed while loading the map.
        public uint GetMapLoadingScreen()
        {
            return mCurrentMap.GetLoadingTexture();
        }

        // ================ PRIVATE ================ //

        // Updates active objects (enemies, player)
        void AdvanceActive(long aMillis)
        {
            foreach (IActive active in mCurrentMap.Active)
            {
                active.Update(aMillis);
            }
        }

        // Updates picking up of objects by enemies and the player
        void AdvancePickups()
        {
            var player = GetMap().Player;

            var entitylist = new List<IEntity>();
            entitylist.AddRange(GetMap().Enemies);
            entitylist.Add(player);

            // for all entities and the player
            foreach (var entity in entitylist)
            {
                Item PickedItem = null;

                foreach (Item item in mCurrentMap.Pickups)
                {
                    int entX  = (int)entity.GetPosition().X;
                    int entY  = (int)entity.GetPosition().Y;
                    int ItemX = (int)item.GetPosition().X;
                    int ItemY = (int)item.GetPosition().Y;

                    if (ItemX == entX && ItemY == entY)
                    {
                        if (item.CanUse(entity))
                        {
                            PickedItem = item;
                            break;
                        }
                    }
                }

                if (PickedItem != null)
                {
                    mCurrentMap.Pickups.Remove(PickedItem);
                    PickedItem.Effect(entity);
                }
            }
        }

        // =========== MEMBERS =========== //
        IMap mCurrentMap;
    }
}

