﻿namespace Wolf
{
    public interface IInput
    {
        public bool MoveForward();      // should player move forwards
        public bool MoveBack();         // should player move back
        public bool MoveLeft();         // should player move left
        public bool MoveRight();        // should player move right
        public bool RotateLeft();       // should player turn left
        public bool RotateRight();      // should player turn right
        public bool Sprint();           // should player sprint
        public bool Equip0();           // should player equip knife
        public bool Equip1();           // should player equip pistol
        public bool Equip2();           // should player equip SMG
        public bool Equip3();           // should player equip BFG
        public bool Shoot();            // should player shoot
        public bool Use();              // should player activate the object in front of him
    }
}
