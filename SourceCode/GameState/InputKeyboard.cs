﻿namespace Wolf
{
    public class InputKeyboard : IInput
    {
        public bool Forward;
        public bool Back;
        public bool Left;
        public bool Right;
        public bool RotLeft;
        public bool RotRight;
        public bool Sprinting;
        public bool Shooting;
        public bool Equipped0;
        public bool Equipped1;
        public bool Equipped2;
        public bool Equipped3;
        public bool Using;

        public bool MoveForward()
        {
            return Forward;
        }

        public bool MoveBack()
        {
            return Back;
        }

        public bool MoveLeft()
        {
            return Left;
        }

        public bool MoveRight()
        {
            return Right;
        }

        public bool RotateLeft()
        {
            return RotLeft;
        }

        public bool RotateRight()
        {
            return RotRight;
        }

        public bool Sprint()
        {
            return Sprinting;
        }

        public bool Equip0()
        {
            return Equipped0;
        }

        public bool Equip1()
        {
            return Equipped1;
        }

        public bool Equip2()
        {
            return Equipped2;
        }

        public bool Equip3()
        {
            return Equipped3;
        }

        public bool Shoot()
        {
            return Shooting;
        }

        public bool Use()
        {
            return Using;
        }
    }
}
