using System;
using System.Collections.Generic;
using System.Numerics;
using BigGustave;

namespace Wolf
{
    // Map loaded from the PNG file
    public class MapPng : IMap
    {
        public MapPng(string aPath) : base(aPath)
        {
            
        }

        // Loads gun pickups
        void LoadGuns()
        {
            Png Knife  = Png.Open(mPath + "knife.png");
            Png Pistol = Png.Open(mPath + "pistol.png");
            Png Smg    = Png.Open(mPath + "smg.png");
            Png Bfg    = Png.Open(mPath + "bfg.png");
            
            GunTextures[0] = LoadTextureFromPngToGpu(Knife, new Vector2(0.0f), new Vector2(Knife.Width, Knife.Height), true);
            GunTextures[1] = LoadTextureFromPngToGpu(Pistol, new Vector2(0.0f), new Vector2(Pistol.Width, Pistol.Height), true);
            GunTextures[2] = LoadTextureFromPngToGpu(Smg, new Vector2(0.0f), new Vector2(Smg.Width, Smg.Height), true);
            GunTextures[3] = LoadTextureFromPngToGpu(Bfg, new Vector2(0.0f), new Vector2(Bfg.Width, Bfg.Height), true);
        }

        // Loads the heads up display
        void LoadHUD()
        {
            Png Hotbar  = Png.Open(mPath + "hotbar.png");
            Png Faces   = Png.Open(mPath + "faces.png");
            Png Numbers = Png.Open(mPath + "numbers.png");
            Png Guns    = Png.Open(mPath + "guns.png");

            HotbarTextures[0] = LoadTextureFromPngToGpu(Hotbar, new Vector2(0.0f), new Vector2(Hotbar.Width, Hotbar.Height), true);
            HotbarTextures[1] = LoadTextureFromPngToGpu(Faces, new Vector2(0.0f), new Vector2(Faces.Width, Faces.Height), true);
            HotbarTextures[2] = LoadTextureFromPngToGpu(Numbers, new Vector2(0.0f), new Vector2(Numbers.Width, Numbers.Height), true);
            HotbarTextures[3] = LoadTextureFromPngToGpu(Guns, new Vector2(0.0f), new Vector2(Guns.Width, Guns.Height), true);
        }

        Dictionary<Vector3, Tuple<int, uint>> HealBindings = new Dictionary<Vector3, Tuple<int, uint>>();

        int AmmoCountPerPickup;
        uint AmmoTextureHandle;
        Vector3 AmmoColor;
        Vector3 SmgColor;
        Vector3 BfgColor;
        uint SmgPickupTexture;
        uint BfgPickupTexture;

        // Loads the pickups
        void LoadPickups()
        {
            Png AmmoBinding = Png.Open(mPath + "ammo_binding.png");
            Png AmmoTex = Png.Open(mPath + "ammo.png");
            Png HealBinding = Png.Open(mPath + "heal_binding.png");
            Png HealTexture = Png.Open(mPath + "heal.png");
            Png GunsPickupTex = Png.Open(mPath + "guns_pickup.png");
            Png GunsPickupBind = Png.Open(mPath + "guns_pickup_binding.png");

            if (AmmoBinding.Width != 1 || AmmoBinding.Height != 2)
                throw new Exception("Ammo binding png must be 1x2!");


            AmmoCountPerPickup = AmmoBinding.GetPixel(0, 1).R;
            AmmoColor = AmmoBinding.GetPixel(0, 0).ToVec3();
            AmmoTextureHandle = LoadTextureFromPngToGpu(AmmoTex, new Vector2(0), new Vector2(AmmoTex.Width, AmmoTex.Height), true);
            AmmoTexture = AmmoTextureHandle;

            SmgColor = GunsPickupBind.GetPixel(0, 0).ToVec3();
            SmgPickupTexture = LoadTextureFromPngToGpu(GunsPickupTex, Vector2.Zero, new Vector2(GunsPickupTex.Width / 2, GunsPickupTex.Height), true);
            BfgColor = GunsPickupBind.GetPixel(1, 0).ToVec3();
            BfgPickupTexture = LoadTextureFromPngToGpu(GunsPickupTex, new Vector2(GunsPickupTex.Width / 2, 0), new Vector2(GunsPickupTex.Width, GunsPickupTex.Height), true);

            if (HealBinding.Height != 2)
                throw new Exception("");

            int HealItemsCount = HealBinding.Width;
            int TexPageWidth = HealTexture.Width / HealItemsCount;
            int TexPageHeight = HealTexture.Height;
            for (int i = 0; i < HealItemsCount; i++)
            {
                Vector3 Color = HealBinding.GetPixel(i, 0).ToVec3();
                int Strength = HealBinding.GetPixel(i, 1).R;
                Vector2 From = new Vector2(TexPageWidth * i, 0);
                Vector2 To = new Vector2(From.X + TexPageWidth, From.Y + TexPageHeight);
                uint Texture = LoadTextureFromPngToGpu(HealTexture, From, To, true);
                HealBindings.Add(Color, Tuple.Create(Strength, Texture));
            }
        }

        // Loads the floor and ceiling colors
        void LoadBackground()
        {
            Png Background = Png.Open(mPath + "background.png");
            if (Background.Width != 1 || Background.Height != 2)
            {
                throw new Exception("Invalid background image format");
            }

            CeilingColor = Background.GetPixel(0, 0).ToVec3();
            FloorColor = Background.GetPixel(0, 1).ToVec3();
            CeilingColor /= 256.0f;
            FloorColor /= 256.0f;
        }

        Dictionary<Vector3, Tuple<uint, uint>> WallColorTextureBinding = new Dictionary<Vector3, Tuple<uint, uint>>();
        Dictionary<Vector3, Tuple<uint, uint>> SecretColorTextureBinding = new Dictionary<Vector3, Tuple<uint, uint>>();
        Dictionary<Vector3, Tuple<uint, uint, uint, uint>> DoorColorTexturesBinding = new Dictionary<Vector3, Tuple<uint, uint, uint, uint>>();
        List<Vector3> WallColors = new List<Vector3>();

        // Loads the textures for the walls
        void LoadWalls()
        {
            Png WallsBind = Png.Open(mPath + "walls_binding.png");
            Png WallsTex = Png.Open(mPath + "walls.png");
            Png DoorsBind = Png.Open(mPath + "doors_binding.png");
            Png DoorsTex = Png.Open(mPath + "doors.png");
            Png ExitBind = Png.Open(mPath + "exit_binding.png");
            Png ExitTex = Png.Open(mPath + "exit.png");
            Png SecretBind = Png.Open(mPath + "secrets_binding.png");
            Png SecretTex = Png.Open(mPath + "secrets.png");

            int TexturesCount = WallsBind.Width;
            int TextureWidth = WallsTex.Width / TexturesCount;
            int TextureHeight = WallsTex.Height / 2;

            for (int i = 0; i < TexturesCount; i++)
            {
                Vector3 Color = WallsBind.GetPixel(i, 0).ToVec3();
                WallColors.Add(Color);
                Vector2 From1 = new Vector2(TextureWidth * i, 0);
                Vector2 To1 = new Vector2(From1.X + TextureWidth, TextureHeight);
                Vector2 From2 = new Vector2(From1.X, From1.Y + TextureHeight);
                Vector2 To2 = new Vector2(To1.X, To1.Y + TextureHeight);
                uint Texture1Handle = LoadTextureFromPngToGpu(WallsTex, From1, To1);
                uint Texture2Handle = LoadTextureFromPngToGpu(WallsTex, From2, To2);

                WallColorTextureBinding.Add(Color,
                    new Tuple<uint, uint>(Texture1Handle, Texture2Handle));
            }

            TexturesCount = 4;
            TextureWidth = DoorsTex.Width / TexturesCount;
            TextureHeight = DoorsTex.Height / 2;

            for (int i = 0; i < 2; i++)
            {
                float Offset = TextureWidth * 2 * i;
                Vector3 Color = DoorsBind.GetPixel(i, 0).ToVec3();
                Vector2 From1 = new Vector2(0 + Offset, 0);
                Vector2 To1 = new Vector2(From1.X + TextureWidth, TextureHeight);
                Vector2 From2 = new Vector2(From1.X, From1.Y + TextureHeight);
                Vector2 To2 = new Vector2(To1.X, To1.Y + TextureHeight);
                Vector2 From3 = new Vector2(From1.X + TextureWidth, From1.Y);
                Vector2 To3 = new Vector2(To1.X + TextureWidth, To1.Y);
                Vector2 From4 = new Vector2(From3.X, From3.Y + TextureHeight);
                Vector2 To4 = new Vector2(To3.X, To3.Y + TextureHeight);
                uint Texture1Handle = LoadTextureFromPngToGpu(DoorsTex, From1, To1);
                uint Texture2Handle = LoadTextureFromPngToGpu(DoorsTex, From2, To2);
                uint Texture3Handle = LoadTextureFromPngToGpu(DoorsTex, From3, To3);
                uint Texture4Handle = LoadTextureFromPngToGpu(DoorsTex, From4, To4);

                DoorColorTexturesBinding.Add(Color, Tuple.Create(Texture1Handle, Texture2Handle, Texture3Handle, Texture4Handle));
            }

            TexturesCount = SecretBind.Width;
            TextureWidth = SecretTex.Width / TexturesCount;
            TextureHeight = SecretTex.Height / 2;

            for (int i = 0; i < TexturesCount; i++)
            {
                Vector3 Color = SecretBind.GetPixel(i, 0).ToVec3();
                Vector2 From1 = new Vector2(TextureWidth * i, 0);
                Vector2 To1 = new Vector2(From1.X + TextureWidth, TextureHeight);
                Vector2 From2 = new Vector2(From1.X, From1.Y + TextureHeight);
                Vector2 To2 = new Vector2(To1.X, To1.Y + TextureHeight);
                uint Texture1Handle = LoadTextureFromPngToGpu(SecretTex, From1, To1);
                uint Texture2Handle = LoadTextureFromPngToGpu(SecretTex, From2, To2);

                SecretColorTextureBinding.Add(Color,
                    new Tuple<uint, uint>(Texture1Handle, Texture2Handle));
            }

            {
                ExitColor = ExitBind.GetPixel(0, 0).ToVec3();
                Vector2 From1 = new Vector2(0, 0);
                Vector2 To1 = new Vector2(From1.X + ExitTex.Width, ExitTex.Height / 2);
                Vector2 From2 = new Vector2(From1.X, To1.Y);
                Vector2 To2 = new Vector2(To1.X, ExitTex.Height);
                uint Texture1Handle = LoadTextureFromPngToGpu(ExitTex, From1, To1);
                uint Texture2Handle = LoadTextureFromPngToGpu(ExitTex, From2, To2);

                ExitTextures = Tuple.Create(Texture1Handle, Texture2Handle);
            }
        }

        Vector3 ExitColor;
        Tuple<uint, uint> ExitTextures;

        Dictionary<Vector3, Tuple<uint, int>> BonusColorTextureBinding = new Dictionary<Vector3, Tuple<uint, int>>();

        // Loads bonuses
        void LoadBonuses()
        {
            Png BonusBind = Png.Open(mPath + "bonus_binding.png");
            Png BonusTex = Png.Open(mPath + "bonus.png");

            var TexturesCount = BonusBind.Width;
            var TextureWidth = BonusTex.Width / TexturesCount;
            var TextureHeight = BonusTex.Height;

            for (int i = 0; i < TexturesCount; i++)
            {
                Vector3 Color = BonusBind.GetPixel(i, 0).ToVec3();
                int Value = BonusBind.GetPixel(i, 1).R;
                Vector2 From = new Vector2(TextureWidth * i, 0);
                Vector2 To = new Vector2(From.X + TextureWidth, TextureHeight);
                uint TextureHandle = LoadTextureFromPngToGpu(BonusTex, From, To, true);

                BonusColorTextureBinding.Add(Color, Tuple.Create(TextureHandle, Value));
            }
        }

        Dictionary<Vector3, uint> ColorBillboardBinding = new Dictionary<Vector3, uint>();

        // Loads billboards (billboard objects in the world)
        void LoadBillboards()
        {
            Png BillBind = Png.Open(mPath + "billboards_binding.png");
            Png BillTex = Png.Open(mPath + "billboards.png");

            var TexturesCount = BillBind.Width;
            var TextureWidth = BillTex.Width / TexturesCount;
            var TextureHeight = BillTex.Height;

            for (int i = 0; i < TexturesCount; i++)
            {
                Vector3 Color = BillBind.GetPixel(i, 0).ToVec3();
                Vector2 From = new Vector2(TextureWidth * i, 0);
                Vector2 To = new Vector2(From.X + TextureWidth, TextureHeight);
                uint TextureHandle = LoadTextureFromPngToGpu(BillTex, From, To, true);

                ColorBillboardBinding.Add(Color, TextureHandle);
            }
        }

        Dictionary<Vector3, uint> ColorSolidBinding = new Dictionary<Vector3, uint>();
        // Loads solid billboards (billboards that cannot be traversed)
        void LoadSolids()
        {
            Png SolidBind = Png.Open(mPath + "solids_binding.png");
            Png SolidTex = Png.Open(mPath + "solids.png");

            var TexturesCount = SolidBind.Width;
            var TextureWidth = SolidTex.Width / TexturesCount;
            var TextureHeight = SolidTex.Height;

            for (int i = 0; i < TexturesCount; i++)
            {
                Vector3 Color = SolidBind.GetPixel(i, 0).ToVec3();
                Vector2 From = new Vector2(TextureWidth * i, 0);
                Vector2 To = new Vector2(From.X + TextureWidth, TextureHeight);
                uint TextureHandle = LoadTextureFromPngToGpu(SolidTex, From, To, true);

                ColorSolidBinding.Add(Color, TextureHandle);
            }
        }

        Vector3 GuardColorS, GuardColorW, GuardColorN, GuardColorE;
        uint GuardTexture;
        List<Vector3> PlayerRotations = new List<Vector3>();

        // Loads player and enemies
        void LoadEntities()
        {
            Png PlayerBind = Png.Open(mPath + "player_rotations.png");
            Png GuardBind = Png.Open(mPath + "guard_binding.png");
            Png GuardTex = Png.Open(mPath + "guard.png");
            
            for (int i = 0; i < 4; i++)
            {
                Vector3 Data = PlayerBind.GetPixel(i, 0).ToVec3();
                PlayerRotations.Add(Data);
            }

            GuardColorS = GuardBind.GetPixel(0, 0).ToVec3();
            GuardColorW = GuardBind.GetPixel(1, 0).ToVec3();
            GuardColorN = GuardBind.GetPixel(2, 0).ToVec3();
            GuardColorE = GuardBind.GetPixel(3, 0).ToVec3();
            GuardTexture = LoadTextureFromPngToGpu(GuardTex, Vector2.Zero, new Vector2(GuardTex.Width, GuardTex.Height), true);
        }

        // Loads the map from a PNG file
        public override void LoadMap()
        {
            if (mLoaded)
            {
                throw new Exception("Map already loaded!");
            }

            // Debug only
            Png DebugPath = Png.Open(Engine.RESOURCES_DIR + "debug_path.png");
            mDebugPathsTexture = LoadTextureFromPngToGpu(DebugPath, Vector2.Zero, new Vector2(DebugPath.Width, DebugPath.Height), true);

            LoadGuns();
            LoadHUD();
            LoadPickups();
            LoadBackground();
            LoadWalls();
            LoadBonuses();
            LoadBillboards();
            LoadSolids();
            LoadEntities();

            Png MapTexture = Png.Open(mPath + "map.png");
            CollisionGrid = new int[MapTexture.Width, MapTexture.Height];

            for (int y = 0; y < MapTexture.Height; y++)
            {
                for (int x = 0; x < MapTexture.Width; x++)
                {
                    Pixel PixelColor = MapTexture.GetPixel(x, y);
                    Vector3 Color = new Vector3(PixelColor.R, PixelColor.G, PixelColor.B);
                    Vector2 Position = new Vector2(x, y);

                    if (WallColorTextureBinding.ContainsKey(Color))    // is wall
                    {
                        PickIboForWall(MapTexture, x, y, WallColors, out uint IBOHandle, out int IndexCount);

                        Tuple<uint, uint> Textures = WallColorTextureBinding[Color];

                        uint TexN = Textures.Item1;
                        uint TexW = Textures.Item2;
                        uint TexS = Textures.Item1;
                        uint TexE = Textures.Item2;

                        Vector3 Above = Vector3.One, Below = Vector3.One, Left = Vector3.One, Right = Vector3.One;
                        if (y - 1 >= 0)
                            Above = MapTexture.GetPixel(x, y - 1).ToVec3();
                        if (y + 1 < MapTexture.Height)
                            Below = MapTexture.GetPixel(x, y + 1).ToVec3();
                        if (x - 1 >= 0)
                            Left = MapTexture.GetPixel(x - 1, y).ToVec3();
                        if (x + 1 < MapTexture.Width)
                            Right = MapTexture.GetPixel(x + 1, y).ToVec3();

                        if (DoorColorTexturesBinding.ContainsKey(Above))
                            TexS = DoorColorTexturesBinding[Above].Item3;
                        if (DoorColorTexturesBinding.ContainsKey(Below))
                            TexN = DoorColorTexturesBinding[Below].Item3;
                        if (DoorColorTexturesBinding.ContainsKey(Left))
                            TexW = DoorColorTexturesBinding[Left].Item4;
                        if (DoorColorTexturesBinding.ContainsKey(Right))
                            TexE = DoorColorTexturesBinding[Right].Item4;

                        IRenderable Wall = new Wall(Position, 
                            Geometry.WallVertexBufferHandle,
                            IBOHandle,
                            IndexCount,
                            TexN, TexW, TexS, TexE);
                        Walls.Add(Wall);

                        int xcoord = (int)(Position.X);
                        int ycoord = (int)(Position.Y);
                        CollisionGrid[xcoord, ycoord] = 1;
                    }
                    else if (DoorColorTexturesBinding.ContainsKey(Color))
                    {
                        Vector3 Top = Vector3.Zero, Left = Vector3.Zero, Bottom = Vector3.Zero, Right = Vector3.Zero;
                        if (y - 1 >= 0)
                            Top = MapTexture.GetPixel(x, y - 1).ToVec3();
                        if (y + 1 < MapTexture.Height)
                            Bottom = MapTexture.GetPixel(x, y + 1).ToVec3();
                        if (x - 1 >= 0)
                            Left = MapTexture.GetPixel(x - 1, y).ToVec3();
                        if (x + 1 < MapTexture.Width)
                            Right = MapTexture.GetPixel(x + 1, y).ToVec3();

                        Door Door = null;

                        if (WallColorTextureBinding.ContainsKey(Top) && WallColorTextureBinding.ContainsKey(Bottom))
                        {
                            // vertical door
                            Door = new Door(Position, this, false, DoorColorTexturesBinding[Color].Item2);
                        }
                        else if (WallColorTextureBinding.ContainsKey(Right) && WallColorTextureBinding.ContainsKey(Left))
                        {
                            // horizontal door
                            Door = new Door(Position, this, true, DoorColorTexturesBinding[Color].Item1);
                        }

                        if (Door != null)
                        {
                            Doors.Add(Door);
                            Usable.Add((IUsable)Door);
                            Active.Add((IActive)Door);
                        }
                    }
                    else if (SecretColorTextureBinding.ContainsKey(Color))
                    {
                        Tuple<uint, uint> Textures = SecretColorTextureBinding[Color];

                        uint TexN = Textures.Item1;
                        uint TexW = Textures.Item2;
                        uint TexS = Textures.Item1;
                        uint TexE = Textures.Item2;

                        Vector3 Above = Vector3.One, Below = Vector3.One;
                        if (y - 1 >= 0)
                            Above = MapTexture.GetPixel(x, y - 1).ToVec3();
                        if (y + 1 < MapTexture.Height)
                            Below = MapTexture.GetPixel(x, y + 1).ToVec3();

                        bool Horizontal = false;

                        if (WallColorTextureBinding.ContainsKey(Above) && WallColorTextureBinding.ContainsKey(Below))
                        {
                            Horizontal = true;
                        }

                        SecretWall Wall = new SecretWall(Position, this, Horizontal, TexN, TexW, TexS, TexE);
                        Walls.Add(Wall);
                        Active.Add(Wall);
                        Usable.Add(Wall);

                        int xcoord = (int)(Position.X);
                        int ycoord = (int)(Position.Y);
                        CollisionGrid[xcoord, ycoord] = 1;
                    }
                    else if (ExitColor == Color)
                    {
                        PickIboForWall(MapTexture, x, y, WallColors, out uint IBOHandle, out int IndexCount);

                        uint TexClosed = ExitTextures.Item1;
                        uint TexOpen = ExitTextures.Item2;

                        Exit Exit = new Exit(Position,
                            Geometry.WallVertexBufferHandle,
                            IBOHandle,
                            IndexCount,
                            TexClosed, TexOpen, this);

                        Walls.Add(Exit);
                        Usable.Add(Exit);

                        int xcoord = (int)(Position.X);
                        int ycoord = (int)(Position.Y);
                        CollisionGrid[xcoord, ycoord] = 1;
                    }
                    else if (ColorBillboardBinding.ContainsKey(Color))
                    {
                        uint TextureHandle = ColorBillboardBinding[Color];
                        IRenderable Bill = new Billboard(Position + new Vector2(0.5f), TextureHandle);
                        Billboards.Add(Bill);
                    }
                    else if (ColorSolidBinding.ContainsKey(Color))
                    {
                        uint TextureHandle = ColorSolidBinding[Color];
                        IRenderable Bill = new Billboard(Position + new Vector2(0.5f), TextureHandle);
                        Billboards.Add(Bill);

                        int xcoord = (int)(Position.X);
                        int ycoord = (int)(Position.Y);
                        CollisionGrid[xcoord, ycoord] = 3;
                    }
                    else if (HealBindings.ContainsKey(Color))
                    {
                        int Strength = HealBindings[Color].Item1;
                        uint TextureHandle = HealBindings[Color].Item2;

                        IRenderable ThisHeal = new Heal(Position + new Vector2(0.5f), TextureHandle, Strength);
                        Pickups.Add(ThisHeal);
                    }
                    else if (AmmoColor == Color)
                    {
                        IRenderable ThisMagazine = new Ammo(Position + new Vector2(0.5f), AmmoTextureHandle, AmmoCountPerPickup);
                        Pickups.Add(ThisMagazine);
                    }
                    else if (BonusColorTextureBinding.ContainsKey(Color))
                    {
                        Tuple<uint, int> TextureValue = BonusColorTextureBinding[Color];
                        Bonus Pickup = new Bonus(Position + new Vector2(0.5f), TextureValue.Item1, TextureValue.Item2);
                        Pickups.Add(Pickup);
                    }
                    else if (SmgColor == Color || BfgColor == Color)
                    {
                        int Ammo = 4;
                        uint Texture;
                        int Index;
                        if (Color == SmgColor)
                        {
                            Texture = SmgPickupTexture;
                            Index = 2;
                        }
                        else
                        {
                            Texture = BfgPickupTexture;
                            Index = 3;
                        }
                        Gun Pickup = new Gun(Position + new Vector2(0.5f), Texture, Index, Ammo);
                        Pickups.Add(Pickup);
                    }
                    else if (PlayerRotations.Contains(Color))
                    {
                        int IndexInArr = PlayerRotations.IndexOf(Color);

                        if (Player != null)
                            throw new Exception("There can be only one player on a map");

                        if (IndexInArr == 0)
                            PlayerStartingFacing = new Vector2(0.0f, 1.0f);
                        else if (IndexInArr == 1)
                            PlayerStartingFacing = new Vector2(-1.0f, 0.0f);
                        else if (IndexInArr == 2)
                            PlayerStartingFacing = new Vector2(0.0f, -1.0f);
                        else
                            PlayerStartingFacing = new Vector2(1.0f, 0.0f);

                        PlayerStartingPosition = Position + new Vector2(0.5f);

                        Player = new Player(PlayerStartingPosition, PlayerStartingFacing);
                        Active.Add(Player);
                    }
                    else if (GuardColorS == Color || GuardColorW == Color || GuardColorN == Color || GuardColorE == Color)
                    {
                        Vector2 Facing;
                        if (Color == GuardColorS)
                            Facing = new Vector2(0.0f, 1.0f);
                        else if (Color == GuardColorW)
                            Facing = new Vector2(-1.0f, 0.0f);
                        else if (Color == GuardColorN)
                            Facing = new Vector2(0.0f, -1.0f);
                        else
                            Facing = new Vector2(1.0f, 0.0f);

                        Guard Guard = new Guard(Position + new Vector2(0.5f), Facing, this, GuardTexture);
                        Enemies.Add(Guard);
                        Active.Add(Guard);
                    }
                }
            }

            if (Player == null)
            {
                throw new Exception("No player on the recently loaded map");
            }

            mLoaded = true;
        }
    }
}

