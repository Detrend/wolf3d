﻿using System.Collections.Generic;
using System.Numerics;

namespace Wolf
{
    // A path an NPC can walk
    public class Path
    {
        public List<Vector2> Points;
        
        public Path()
        {
            Points = new List<Vector2>();
        }

        public bool IsEmpty()
        {
            return Points.Count == 0;
        }
    }
}
