﻿using System.Numerics;

namespace Wolf
{
    // Item dropped on the ground. Might be an ammo magazine, heal or some bonus
    // Most items can be picked only by the player, but some are also pickable by the guards
    class Item : Billboard, IPickup
    {
        public Item(Vector2 aPosition, uint aTexture)
        : base(aPosition, aTexture)
        {
            
        }

        // How does the pickup effect the entity?
        public virtual void Effect(IEntity ent)
        {

        }

        // Can the given entity use this pickup?
        public virtual bool CanUse(IEntity ent)
        {
            return true;
        }
    }
}
