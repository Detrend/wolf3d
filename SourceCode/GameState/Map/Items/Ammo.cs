﻿using System;
using System.Numerics;

namespace Wolf
{
    class Ammo : Item
    {
        public Ammo(Vector2 aPosition, uint aTexture, int aCount)
        : base(aPosition, aTexture)
        {
            mBulletCount = aCount;
        }

        // Gives player some ammo
        // Does nothing for guards
        public override void Effect(IEntity ent)
        {
            if (!(ent is Player))
            {
                return;
            }

            var player = ent as Player;

            Random Rand = new Random();
            int Ammo = Rand.Next(1, mBulletCount + 1);
            player.GetData().Ammo += Ammo;
            player.GetData().ScoreGlow = 0.25f;

            Engine.GetSound().PlaySound(SoundType.Ammo);
        }

        public override bool CanUse(IEntity ent)
        {
            return ent is Player;
        }

        private int mBulletCount;
    }
}
