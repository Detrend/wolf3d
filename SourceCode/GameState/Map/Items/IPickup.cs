﻿namespace Wolf
{
    interface IPickup
    {
        void Effect(IEntity entity);

        bool CanUse(IEntity entity);
    }
}
