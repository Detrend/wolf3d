﻿using System.Numerics;

namespace Wolf
{
    class Heal : Item
    {
        private int mHealthRestore;

        public Heal(Vector2 aPosition, uint aTexture, int aStrength) : base(aPosition, aTexture)
        {
            mHealthRestore = aStrength;
        }

        // Gives player some HP
        // Maxes out the HP of guards
        public override void Effect(IEntity ent)
        {
            Engine.GetSound().PlaySound(SoundType.Pickup);

            if (ent is Player)
            {
                var player = ent as Player;
                var data = player.GetData();
                if (data.HP < 20)
                {
                    Engine.GetSound().PlaySound(SoundType.Heal);
                }

                data.HP += mHealthRestore;
                if (data.HP > 100)
                    data.HP = 100;
                data.ScoreGlow = 0.25f;
            }
            else if (ent is Guard)
            {
                var guard = ent as Guard;
                guard.mHP = Guard.GUARD_MAX_HP;
            }
        }

        // Both player and guards can use this
        public override bool CanUse(IEntity ent)
        {
            if (ent is Player)
            {
                var player = ent as Player;
                return player.GetData().HP < 100;
            }
            else if (ent is Guard)
            {
                return true;
            }
            return false;
        }
    }
}
