﻿using System.Numerics;

namespace Wolf
{
    // Rewards player with a score
    class Bonus : Item
    {
        public Bonus(Vector2 aPosition, uint aTexture, int aValue)
        : base(aPosition, aTexture)
        {
            mValue = aValue;
        }

        // Increments the player's bonus counter
        public override void Effect(IEntity ent)
        {
            if (!(ent is Player))
            {
                return;
            }

            var player = ent as Player;

            player.GetData().Score += mValue;
            player.GetData().ScoreGlow = 0.25f;

            Engine.GetSound().PlaySound(SoundType.Pickup);
        }

        // Only player can pickup bonuses
        public override bool CanUse(IEntity ent)
        {
            return ent is Player;
        }

        private int mValue;
    }
}
