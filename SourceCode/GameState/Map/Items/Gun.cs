﻿using System.Numerics;

namespace Wolf
{
    // Gun. Player can pick it up
    class Gun : Item
    {
        public Gun(Vector2 aPosition, uint aTexture, int aIndex, int aAmmo)
        : base(aPosition, aTexture)
        {
            mGunIndex = aIndex;
            mAmmo     = aAmmo;
        }

        // Gives plyaer the appropriate gun
        public override void Effect(IEntity ent)
        {
            if (!(ent is Player))
            {
                return;
            }

            var player = ent as Player;
            player.GetData().HasGun[mGunIndex] = true;
            player.GetData().Ammo += mAmmo;
            player.GetData().ScoreGlow = 0.25f;

            Engine.GetSound().PlaySound(SoundType.WeaponPickup);
        }

        // Only for the player
        public override bool CanUse(IEntity ent)
        {
            return ent is Player;
        }

        private int mGunIndex;  // type of a gun
        private int mAmmo;      // how much ammo it gives us
    }
}
