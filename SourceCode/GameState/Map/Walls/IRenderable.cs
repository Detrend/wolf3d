﻿using System.Numerics;

namespace Wolf
{
    // 3D object in the world that can be rendered
    public interface IRenderable
    {
        // Returns a vertex buffer handle of this object
        public uint GetVB();

        // Returns a index buffer handle of this object
        public uint GetIB();

        // Returns the number of indices of this object that OpenGL should render
        public int GetIndicesCount();

        // Returns a set of textures this object has. It might have one or more textures
        public uint[] GetTextures();

        // Returns the world position of this object
        public Vector2 GetPosition();
    }
}
