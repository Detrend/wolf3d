﻿using System;
using System.Numerics;

namespace Wolf
{
    // Secret wall object
    // If activated then it moves back and potentially reveals a secret
    class SecretWall : IRenderable, IUsable, IActive
    {
        // =============== PUBLIC ================ //
        public SecretWall(Vector2 aPosition, IMap aMap, bool aHorizontal, uint aTexN, uint aTexW, uint aTexS, uint aTexE)
        {
            mPosition = aPosition;
            mHorizontal = aHorizontal;
            mMap = aMap;

            mTextures = new uint[] { aTexN, aTexW, aTexS, aTexE };
            mProgress = Vector2.Zero;
            mDirection = Vector2.Zero;

            mOpening = false;
            mUsed = false;
        }

        public uint GetVB()
        {
            return Geometry.WallVertexBufferHandle;
        }

        public uint GetIB()
        {
            return Geometry.WallIndexBufferHandle[1, 1, 1, 1];
        }

        public int GetIndicesCount()
        {
            return Geometry.WallIndicesCount[1, 1, 1, 1];
        }

        public uint[] GetTextures()
        {
            return mTextures;
        }

        public Vector2 GetPosition()
        {
            return mPosition + mProgress;
        }

        public bool CanUse(Player aPlayer)
        {
            if (mUsed)
                return false;

            Vector2 Facing = aPlayer.GetFacing();
            Vector2 Position = aPlayer.GetPosition();

            int PointX = (int)(Facing + Position).X;
            int PointY = (int)(Facing + Position).Y;
            int GridX = (int)mPosition.X;
            int GridY = (int)mPosition.Y;

            if (PointX == GridX && PointY == GridY)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Use(Player aPlayer)
        {
            if (mHorizontal)
            {
                if (aPlayer.GetPosition().X > mPosition.X)
                {
                    // move to left
                    mDirection = new Vector2(-1.0f, 0.0f);
                }
                else
                {
                    // move to right
                    mDirection = new Vector2(1.0f, 0.0f);
                }
            }
            else
            {
                if (aPlayer.GetPosition().Y > mPosition.Y)
                {
                    // move up
                    mDirection = new Vector2(0.0f, -1.0f);
                }
                else
                {
                    // move down
                    mDirection = new Vector2(0.0f, 1.0f);
                }
            }

            mOpening = true;
            mUsed = true;

            Engine.GetSound().PlaySound(SoundType.Secret);
        }

        public void Update(long aMs)
        {
            if (mOpening)
            {
                float Progress = mHorizontal ? Math.Abs(mProgress.X) : Math.Abs(mProgress.Y);

                if (Progress < 1.0f)
                {
                    // move further
                    float Delta = aMs / (1000.0f / 60.0f);
                    mProgress += mDirection * 0.01f * Delta;
                }
                
                if (Progress >= 1.0f)
                {
                    mProgress = Vector2.Normalize(mProgress);
                    int OldGridX = (int)mPosition.X;
                    int OldGridY = (int)mPosition.Y;
                    int NewGridX = (int)(mPosition + mProgress).X;
                    int NewGridY = (int)(mPosition + mProgress).Y;

                    mMap.CollisionGrid[OldGridX, OldGridY] = 0;     //  remove collision box at old coordinates
                    mMap.CollisionGrid[NewGridX, NewGridY] = 1;     //  add collision box to new coordinates
                    mPosition += mProgress;

                    int NextGridX = (int)(mPosition + mProgress).X;
                    int NextGridY = (int)(mPosition + mProgress).Y;

                    mProgress = Vector2.Zero;

                    if (mMap.CollisionGrid[NextGridX, NextGridY] != 0)  // if there is something in our way then stop
                    {
                        mOpening = false;
                    }
                }
            }
        }

        // ================== PRIVATE ================== //
        private Vector2 mPosition;
        private uint[]  mTextures;
        private bool    mOpening;
        private bool    mUsed;
        private bool    mHorizontal;
        private Vector2 mProgress;
        private Vector2 mDirection;
        private IMap    mMap;
    }
}
