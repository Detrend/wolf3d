﻿using System;
using System.Numerics;

namespace Wolf
{
    // Door slides to the side if player or enemy activates it
    // It acts like a wall until fully opened
    class Door : IRenderable, IActive, IUsable
    {
        public Door(Vector2 aPosition, IMap aMap, bool aHorizontal, uint aTexture, long aOpenDuration = 10000)
        {
            mPosition = aPosition;
            mHorizontal = aHorizontal;
            mTexture = aTexture;
            mOpenDuration = aOpenDuration;
            mOpenProgress = 0.0f;
            mMap = aMap;

            int GridX = (int)mPosition.X;
            int GridY = (int)mPosition.Y;
            mMap.CollisionGrid[GridX, GridY] = 2;
        }

        public uint GetVB()
        {
            if (mHorizontal)
                return Geometry.HorizontalDoorVertexBuffer;
            else
                return Geometry.VerticalDoorVertexBuffer;
        }

        public uint GetIB()
        {
            return Geometry.DoorIndexBuffer;
        }

        public int GetIndicesCount()
        {
            return Geometry.DoorIndicesCount;
        }

        public uint[] GetTextures()
        {
            return new uint[1] { mTexture };
        }

        public Vector2 GetPosition()
        {
            Vector2 Offset;
            if (mHorizontal)
                Offset = new Vector2(Math.Clamp(mOpenProgress, 0.0f, 1.0f), 0.0f);
            else
                Offset = new Vector2(0.0f, Math.Clamp(mOpenProgress, 0.0f, 1.0f));

            return (mPosition + Offset);
        }

        public bool CanUse(Player aPlayer)
        {
            Vector2 Facing = aPlayer.GetFacing();
            Vector2 Position = aPlayer.GetPosition();

            int PointX = (int)(Facing + Position).X;
            int PointY = (int)(Facing + Position).Y;
            int GridX = (int)mPosition.X;
            int GridY = (int)mPosition.Y;

            if (PointX == GridX && PointY == GridY)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Use(Player aPlayer)
        {
            Open();
            Engine.GetAI().StreamEvent(AIEvent.DoorOpen, mPosition, mMap);
        }

        public void Open()
        {
            if (mCloseCountDown == 0)
            {
                Engine.GetSound().PlaySound(SoundType.Door);
            }

            mCloseCountDown = mOpenDuration;
        }

        public void Update(long aMs)
        {
            if (mCloseCountDown > 0)
            {
                // door is open / is opening
                mCloseCountDown -= aMs;
                if (mOpenProgress < 1.0f)
                {
                    float Delta = aMs / (1000.0f / 60.0f);
                    mOpenProgress += 0.02f * Delta;
                }
                else
                {
                    mOpenProgress = 1.0f;
                    int GridX = (int)mPosition.X;
                    int GridY = (int)mPosition.Y;
                    mMap.CollisionGrid[GridX, GridY] = 0;
                }
            }
            else
            {
                // door should close / is closing
                int PointX = (int)mMap.Player.GetPosition().X;
                int PointY = (int)mMap.Player.GetPosition().Y;
                int GridX = (int)mPosition.X;
                int GridY = (int)mPosition.Y;

                bool ShouldOpen = (PointX == GridX && PointY == GridY);
                // cycle through enemies and check if none is colliding with the door
                foreach (Guard Dude in mMap.Enemies)
                {
                    int PosX = (int)Dude.GetPosition().X;
                    int PosY = (int)Dude.GetPosition().Y;
                    if (mHorizontal)
                    {
                        if (GridY == PosY && (GridX == PosX || GridX + 1 == PosX || GridX - 1 == PosX))
                        {
                            ShouldOpen = true;
                        }
                    }
                    else
                    {
                        if (GridX == PosX && (GridY == PosY || GridY + 1 == PosY || GridY - 1 == PosY))
                        {
                            ShouldOpen = true;
                        }
                    }
                }

                if (ShouldOpen)
                {
                    Open();
                }
                else
                {
                    if (mOpenProgress > 0)
                    {
                        mCloseCountDown = 0;
                        float Delta = aMs / (1000.0f / 60.0f);
                        mOpenProgress -= 0.02f * Delta;
                    }
                    else
                    {
                        mOpenProgress = 0;
                        mMap.CollisionGrid[GridX, GridY] = 2;
                    }
                }
            }
        }

        // =============== PRIVATE ===============
        private bool    mHorizontal;
        private float   mOpenProgress;
        private uint    mTexture;
        private Vector2 mPosition;
        private long    mCloseCountDown;
        private long    mOpenDuration;
        private IMap    mMap;
    }
}
