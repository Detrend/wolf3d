﻿using System.Numerics;

namespace Wolf
{
    // Billboard is a 2D plane rotated towards camera
    public class Billboard : IRenderable
    {
        public Billboard(Vector2 aPosition, uint aTexture)
        {
            mPosition = aPosition;
            mTexture = aTexture;
        }

        public virtual uint GetVB()
        {
            return Geometry.BillboardVertexBufferHandle;
        }

        public virtual uint GetIB()
        {
            return Geometry.BillboardIndexBufferHandle;
        }

        public virtual int GetIndicesCount()
        {
            return Geometry.BillboardIndicesCount;
        }

        public virtual uint[] GetTextures()
        {
            return new uint[1] { mTexture };
        }

        public virtual int[] GetSheetIndices()
        {
            return new int[] { 0, 0 };
        }

        public virtual Vector2 GetPosition()
        {
            return mPosition;
        }

        public virtual Vector2 GetFacing()
        {
            return Vector2.Zero;
        }

        private Vector2 mPosition;
        private uint    mTexture;
    }
}
