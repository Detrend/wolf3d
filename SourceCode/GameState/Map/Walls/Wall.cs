﻿using System.Numerics;

namespace Wolf
{
    // Static wall object. Player and guards cannot traverse it.
    class Wall : IRenderable
    {
        public Wall(Vector2 aPosition, uint aVertexBuffer, uint aIndexBuffer, int aIndicesCount, uint aTexN, uint aTexW, uint aTexS, uint aTexE)
        {
            mPosition = aPosition;
            mVertexBuffer = aVertexBuffer;
            mIndexBuffer = aIndexBuffer;
            mIndicesCount = aIndicesCount;
            mTextures = new uint[] { aTexN, aTexW, aTexS, aTexE };
        }

        public uint GetVB()
        {
            return mVertexBuffer;
        }

        public uint GetIB()
        {
            return mIndexBuffer;
        }

        public int GetIndicesCount()
        {
            return mIndicesCount;
        }

        public uint[] GetTextures()
        {
            return mTextures;
        }

        public Vector2 GetPosition()
        {
            return mPosition;
        }

        private uint    mVertexBuffer;
        private uint    mIndexBuffer;
        private int     mIndicesCount;
        private Vector2 mPosition;
        private uint[]  mTextures;
    }
}
