﻿using System.Numerics;

namespace Wolf
{
    // The level is finished when player activates the exit
    class Exit : IRenderable, IUsable
    {
        // ============= PUBLIC ============= //
        public Exit(Vector2 aPosition, uint aVertexBuffer, uint aIndexBuffer, int aIndexCount, uint aTexClosed, uint aTexOpen, IMap aMap)
        {
            mPosition = aPosition;

            mVertexBuffer = aVertexBuffer;
            mIndexBuffer = aIndexBuffer;
            mIndicesCount = aIndexCount;

            mOpen = false;
            mTextureClosed = aTexClosed;
            mTextureOpen = aTexOpen;
            mMap = aMap;
        }

        public uint GetVB()
        {
            return mVertexBuffer;
        }

        public uint GetIB()
        {
            return mIndexBuffer;
        }

        public int GetIndicesCount()
        {
            return mIndicesCount;
        }

        public uint[] GetTextures()
        {
            if (mOpen)
                return new uint[] { mTextureOpen, mTextureOpen, mTextureOpen, mTextureOpen };
            else
                return new uint[] { mTextureClosed, mTextureClosed, mTextureClosed, mTextureClosed };
        }

        public Vector2 GetPosition()
        {
            return mPosition;
        }

        public bool CanUse(Player aPlayer)
        {
            if (mOpen)
                return false;

            Vector2 Facing = aPlayer.GetFacing();
            Vector2 Position = aPlayer.GetPosition();

            int PointX = (int)(Facing + Position).X;
            int PointY = (int)(Facing + Position).Y;
            int GridX = (int)mPosition.X;
            int GridY = (int)mPosition.Y;

            if (PointX == GridX && PointY == GridY)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Use(Player aPlayer)
        {
            mOpen = true;
            mMap.Finished = true;
        }

        // =============== PRIVATE ============ //
        private Vector2 mPosition;
        private uint    mVertexBuffer;
        private uint    mIndexBuffer;
        private int     mIndicesCount;
        private bool    mOpen;
        private uint    mTextureOpen;
        private uint    mTextureClosed;
        private IMap    mMap;
    }
}
