﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Numerics;
using System.Data;

namespace Wolf
{
    public class Player : IEntity, IActive
    {
        Vector2     mPosition;      // current position of the player on the level grid
        Vector2     mFacing;        // direction the player is facing. Unit vector
        PlayerData  mData;          // player data

        // Respawn data
        Vector2 mSpawnPosition;
        Vector2 mSpawnFacing;

        // Constructs the player on the given position rotated towards the aFacing vector
        public Player(Vector2 aPosition, Vector2 aFacing)
        {
            mSpawnPosition = aPosition;
            mPosition      = aPosition;
            mFacing        = aFacing;
            mSpawnFacing   = aFacing;
            mData = new PlayerData(100, 0, 8, 1);
        }

        // Returns the player data
        public PlayerData GetData()
        {
            return mData;
        }

        // Updates the player
        public void Update(long aMillis)
        {
            var aPlayerInput = Engine.GetInputSystem().GetInput();

            PlayerData data = Engine.GetScene().GetPlayerData();

            Vector3 playerFacing = new Vector3(GetFacing().X, 0.0f, GetFacing().Y);
            Vector3 up           = new Vector3(0.0f, 1.0f, 0.0f);
            Vector3 playerSide   = Vector3.Cross(up, playerFacing);
            Vector2 side         = Vector2.Normalize(new Vector2(playerSide.X, playerSide.Z));

            float Delta = aMillis / (1000.0f / 60.0f);
            const float MoveDistance = 0.08f;
            float ShootingSpeed = data.GunShootingSpeed[data.CurrentGun];

            bool Locked = false;

            if (data.HP <= 0) // player is dead
            {
                Locked = true;
                if (data.HurtGlow <= 3.0f)
                {
                    data.HurtGlow += 0.05f * Delta;
                }
                else
                {
                    Respawn();
                    data.Lives -= 1;
                    data.HurtGlow = 0.0f;
                    data.HP = 100;
                    Locked = false;
                }
            }

            if (data.GunIndex > 0.0f)
                data.GunIndex += ShootingSpeed * Delta;

            if (data.GunIndex >= 5.0f)
            {
                data.GunIndex = 0.0f;

                if (data.GunRepeatedShooting[data.CurrentGun])
                {
                    data.CanShoot = true;
                }

                if (data.Ammo <= 0)
                {
                    data.CurrentGun = 0;
                }
            }

            data.HurtGlow -= 0.015f * Delta;
            if (data.HurtGlow < 0.0f)
                data.HurtGlow = 0.0f;
            data.ScoreGlow -= 0.015f * Delta;
            if (data.ScoreGlow < 0.0f)
                data.ScoreGlow = 0.0f;

            data.FaceIndex += 0.1f * Delta;
            if (data.FaceIndex >= 10.0f)
                data.FaceIndex = 0.0f;

            if (Locked == false)
            {
                float moveMult = 1.0f;
                if (aPlayerInput.Sprint())
                {
                    moveMult = 1.7f;
                }

                if (aPlayerInput.MoveForward())
                {
                    Move(GetFacing() * MoveDistance * moveMult * Delta);
                }

                if (aPlayerInput.MoveBack())
                {
                    Move(GetFacing() * -MoveDistance * moveMult * Delta);
                }

                if (aPlayerInput.MoveLeft())
                {
                    Move(side * MoveDistance * moveMult * Delta);
                }

                if (aPlayerInput.MoveRight())
                {
                    Move(side * -MoveDistance * moveMult * Delta);
                }

                if (aPlayerInput.RotateLeft())
                {
                    Rotate(3.0f * Delta);
                }

                if (aPlayerInput.RotateRight())
                {
                    Rotate(-3.0f * Delta);
                }

                if (aPlayerInput.Equip0())
                {
                    if (data.HasGun[0])
                    {
                        data.CurrentGun = 0;
                    }
                }

                if (aPlayerInput.Equip1())
                {
                    if (data.HasGun[1])
                    {
                        data.CurrentGun = 1;
                    }
                }

                if (aPlayerInput.Equip2())
                {
                    if (data.HasGun[2])
                    {
                        data.CurrentGun = 2;
                    }
                }

                if (aPlayerInput.Equip3())
                {
                    if (data.HasGun[3])
                    {
                        data.CurrentGun = 3;
                    }
                }

                if (aPlayerInput.Shoot())
                {
                    if (data.GunIndex == 0)
                    {
                        int Consumption = data.GunAmmoConsumption[data.CurrentGun];
                        if (data.Ammo >= Consumption && data.CanShoot)
                        {
                            SoundType Sound = data.GunSounds[data.CurrentGun];
                            float Speed  = data.GunShootingSpeed[data.CurrentGun];
                            int   Damage = data.GunDamage[data.CurrentGun];
                            float Range  = data.GunRange[data.CurrentGun];

                            data.GunIndex = Speed;
                            bool Falloff = (data.CurrentGun != 0);
                            Shoot(Damage, Range, Sound, Falloff);
                            if (data.CurrentGun != 0)
                            {
                                Engine.GetAI().StreamEvent(AIEvent.Shooting, mPosition, Engine.GetScene().GetMap());
                            }
                            data.Ammo -= Consumption;
                            data.CanShoot = false;
                        }
                    }
                }
                else
                {
                    data.CanShoot = true;
                }

                if (aPlayerInput.Use())
                {
                    foreach (IUsable Item in Engine.GetScene().GetMap().Usable)
                    {
                        if (Item.CanUse(this))
                        {
                            Item.Use(this);
                            break;
                        }
                    }
                }
            }
        }

        // Respawns the player in the same place he started before
        public void Respawn()
        {
            mPosition = mSpawnPosition;
            mFacing   = mSpawnFacing;
        }

        public void Move(Vector2 aHowMuch)
        {
            var map = Engine.GetScene().GetMap();

            int GridX = (int)mPosition.X;
            int GridY = (int)mPosition.Y;

            bool xstop = false, ystop = false;
            float hitboxSize = 0.25f;
            float plusSizeX = Math.Sign(aHowMuch.X) * hitboxSize;
            float plusSizeY = Math.Sign(aHowMuch.Y) * hitboxSize;

            // x
            GridX = (int)(mPosition.X + aHowMuch.X + plusSizeX);
            GridY = (int)(mPosition.Y);
            if (map.CollisionGrid[GridX, GridY] > 0)
            {
                xstop = true;
                aHowMuch.X = 0;
            }

            // y
            GridX = (int)(mPosition.X);
            GridY = (int)(mPosition.Y + aHowMuch.Y + plusSizeY);
            if (map.CollisionGrid[GridX, GridY] > 0)
            {
                ystop = true;
                aHowMuch.Y = 0;
            }

            // both
            GridX = (int)(mPosition.X + aHowMuch.X + plusSizeX);
            GridY = (int)(mPosition.Y + aHowMuch.Y + plusSizeY);
            if (map.CollisionGrid[GridX, GridY] > 0 && !xstop && !ystop)
            {
                if (Vector2.Abs(aHowMuch).X > Vector2.Abs(aHowMuch).Y)
                {
                    aHowMuch.Y = 0;
                }
                else
                {
                    aHowMuch.X = 0;
                }
            }

            mPosition += aHowMuch;
        }

        public void Rotate(float aHowMuchInDegrees)
        {
            Vector4 Facing = new Vector4(mFacing.X, 0.0f, mFacing.Y, 1.0f);
            Facing = Vector4.Transform(Facing, Matrix4x4.CreateRotationY(aHowMuchInDegrees / 180.0f * (float)Math.PI));
            mFacing.X = Facing.X;
            mFacing.Y = Facing.Z;
        }

        public void Shoot(int aDamage, float aRange, SoundType soundEffect, bool aFalloff = false)
        {
            var map = Engine.GetScene().GetMap();

            var soundSystem = Engine.GetSound();
            soundSystem.PlaySound(soundEffect);

            bool Damaged = false;

            foreach (Guard Dude in map.Enemies)
            {
                if (Dude.IsDead())
                    continue;

                Vector2 DudePos = Dude.GetPosition();
                Vector2 ToDude  = DudePos - mPosition;

                if (ToDude.Length() > aRange)
                    continue;

                float Dot = Vector2.Dot(Vector2.Normalize(ToDude), Vector2.Normalize(mFacing));
                if (map.CanHit(mPosition, DudePos))
                {
                    if (Dot > 0.95f && !Damaged)
                    {
                        // hit him
                        Random Rand = new Random();
                        int HurtRand = Rand.Next(0, 100);
                        bool Hurt = (HurtRand > 50);
                        int Damage = aDamage;
                        if (aFalloff)
                            Damage = (int)(aDamage * (1.0f - ToDude.Length() / aRange));
                        Dude.Hurt(Damage, Hurt);
                        Damaged = true;
                    }
                }
            }
        }

        // Damages the player
        public void Hurt(int aDamage)
        {
            var state = Engine.GetScene();
            state.GetPlayerData().HP -= aDamage;

            Engine.GetSound().PlaySound(SoundType.PlayerPain);

            if (state.GetPlayerData().HP <= 0)
            {
                Engine.GetSound().PlaySound(SoundType.PlayerDeath);
            }

            if (state.GetPlayerData().HurtGlow < 0.5f)
                state.GetPlayerData().HurtGlow = 0.5f;
        }

        // Returns the position of the player on level grid
        public Vector2 GetPosition()
        {
           return mPosition;
        }

        // Returns the direction the player is facing
        public Vector2 GetFacing()
        {
            return Vector2.Normalize(mFacing);
        }
    }
}
