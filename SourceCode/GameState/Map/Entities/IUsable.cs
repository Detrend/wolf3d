﻿namespace Wolf
{
    // Usables are objects that can be activated by the player
    // by pressing the space
    public interface IUsable
    {
        public bool CanUse(Player aPlayer);

        public void Use(Player aPlayer);
    }
}
