﻿using System.Numerics;

namespace Wolf
{
    // Player and enemies are entities
    interface IEntity
    {
        // Returns the 2D position of the entity in the level
        public Vector2 GetPosition();

        // Returns the direction this entity is facing towards. Should be normalized
        public Vector2 GetFacing();
    }
}
