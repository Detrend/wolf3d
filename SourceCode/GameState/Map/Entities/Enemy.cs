﻿using System.Numerics;

namespace Wolf
{
    // // // // // //
    // ENEMY CLASS //
    // // // // // //
    // IEntity component is for rendering, IActive is for updating data
    public abstract class Enemy : Billboard, IEntity, IActive
    {
        public Enemy(uint aTextureSheet)
        : base(Vector2.Zero, aTextureSheet)
        {

        }

        // Deals aDamage health points of damage to this enemy.
        // If aAnimation is true then the entity should get stuck in pain animation for a while
        abstract public void Hurt(int aDamage, bool aAnimation);

        // IEntity
        abstract public override Vector2 GetPosition();
        abstract public override Vector2 GetFacing();
        //~IEntity

        // Billboard
        abstract public override int[] GetSheetIndices();
        //~Billboard

        // IActive
        abstract public void Update(long aMs);
        //~IActive
    }
}
