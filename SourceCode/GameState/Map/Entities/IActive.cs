﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wolf
{
    public interface IActive
    {
        public void Update(long aMs);
    }
}
