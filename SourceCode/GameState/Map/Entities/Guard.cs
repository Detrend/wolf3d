﻿using System;
using System.Numerics;

namespace Wolf
{
    // Guard state machine states
    public enum State
    {
        Still = 0,      // does not know anything about the player
        Alert = 1,      // player run away and can't see him
        Dead  = 2       // killed by the player, inactive
    }

    // // // // // //
    // GUARD CLASS //
    // // // // // //
    public class Guard : Enemy
    {
        // Read only interface
        public State    mState             { get; private set; }  // current state
        public Vector2  mLastSeenPlayerPos { get; private set; }  // last player position
        public AIPlan   mPlan              { get; private set; }  // the current plan
        public Path     mPath              { get; private set; }  // the path this guard is currently moving on

        public Vector2 spawnPosition { get; private set; }  // the position we were spawned at
        public Vector2 spawnFacing   { get; private set; }

        // LOCATION
        Vector2 mPosition;  // current position
        Vector2 mFacing;    // current direction
        IMap    mMap;       // current map

        // ANIMATIONS
        // ==========
        // These are the number of frames of respective animations
        static readonly int[] ANIM_INDICES_COUNT = new int[] { 1, 4, 1, 5, 1, 2, 1, 1 };

        float   mAnimationIndex;        // The index of current animation (in seconds)
        float   mAnimationCountdown;    // This is how long one animation should remain active (even if it ends)

        // Animations available for the guard
        enum Animation
        { 
            Still = 0,
            Walking = 1,
            Hurt = 2,
            Death = 3,
            Dead = 4,
            Attack = 5,
            Alert = 6,
            Aiming = 7
        }

        // This is the name of currently running animation. Index into the array above to see
        // how many frames long this animations is
        Animation mCurrentAnimation;
        // ==========

        // PLANNING
        AIPlan.PlanStep? mCurrentPlanStep;  // the step that is being currently executed
        bool mMovingOnPath;

        // STATS
        public static readonly float MAX_DISTANCE_TO_COLLECT_PICKUP = 15.0f;    // this distance the guard is willing to walk to collect pickup
        public static readonly int   GUARD_MAX_HP = 100;

        static readonly int   GUARD_DAMAGE    = 20;
        static readonly float GUARD_MAX_RANGE = 100.0f;

        public int mHP { get; set; }

        int   mDamage = GUARD_DAMAGE;
        float mRange  = GUARD_MAX_RANGE;

        readonly static float FORGET_TIME = 15.0f;

        float mTimeSinceLastAchtung    = 0.0f;  // time since this guard allerted his surrounding the last time
        float mTimeSinceLastSeenPlayer = 0.0f;  // time since this guard saw the player

        // Randomizer for this guard
        Random  mRand;

        // =============== PUBLIC =============== //
        public Guard(Vector2 aPosition, Vector2 aFacing, IMap aMap, uint aTexture) : base(aTexture)
        {
            mCurrentAnimation = Animation.Still;
            mAnimationIndex = 0.0f;
            mState = State.Still;

            mHP = GUARD_MAX_HP;
            mPosition = aPosition;
            mFacing   = aFacing;
            mMap      = aMap;
            mLastSeenPlayerPos = Vector2.Zero;

            spawnPosition = aPosition;
            spawnFacing   = aFacing;

            mRand = new Random();
            mPath = new Path();
            mMovingOnPath = false;
        }

        // Updates the guard animation, state and movement
        public override void Update(long aMs)
        {
            UpdateAnimations(aMs);
            UpdateAi(aMs);
            UpdateMovement(aMs);
        }

        // Changes the current plan
        public void SetPlan(AIPlan newPlan)
        {
            mPlan = newPlan;
            mCurrentPlanStep = null;
        }

        // Checks whether this guard is dead
        public bool IsDead()
        {
            return mHP <= 0;
        }

        // === IEntity ===
        // Returns the position of the guard on the grid
        public override Vector2 GetPosition()
        {
            return mPosition;
        }

        // Returns the direction the guard is facing
        public override Vector2 GetFacing()
        {
            return mFacing;
        }
        // ===~IEntity ===

        // Rotates the guards towards given location
        public void FaceTowards(Vector2 point)
        {
            var direction = point - mPosition;
            if (direction.LengthSquared() > 0.0001f)
            {
                mFacing = Vector2.Normalize(direction);
            }
        }

        // Damages the guard by certain amount of HP
        // if aAnimation is true then the guard also plays hit animation
        public override void Hurt(int aDamage, bool aAnimation)
        {
            Engine.GetSound().PlaySound(SoundType.EnemyPain);

            mState = State.Alert;
            FacePlayer();

            const float HIT_ANIM_LEN = 2.0f;
            if (aAnimation)
            {
                StartAnimation(Animation.Hurt, HIT_ANIM_LEN);
            }

            StopMoving();

            mHP -= aDamage;

            if (mHP <= 0)   // kill him
            {
                Engine.GetSound().PlaySound(SoundType.EnemyDeath);

                int GridX = (int)mPosition.X;
                int GridY = (int)mPosition.Y;

                // Spawn ammo magazine
                Vector2 DeathLocation = new Vector2(GridX + 0.5f, GridY + 0.5f);
                Ammo Magazine = new Ammo(DeathLocation, mMap.AmmoTexture, 8);
                mMap.Pickups.Add(Magazine);

                mState = State.Dead;

                // Starts the death animation
                StartAnimation(Animation.Death, 0.0f);
            }
        }

        // Returns which indices on the guard sheet should be picked for rendering
        public override int[] GetSheetIndices()
        {
            ChooseTextureIndices(out int IndexX, out int IndexY);
            return new int[] {8, 7, IndexX, IndexY };
        }

        // Checks if the player can be seen from the current position of the guard
        public bool CanSeePlayer()
        {
            if (mMap.CanHit(mPosition, mMap.Player.GetPosition()))
            {
                return true;
            }

            return false;
        }

        // Can guard see the given point if he turns towards it?
        public bool CanSeePosition(Vector2 pos)
        {
            var myPos = GetPosition();
            return mMap.CanHit(myPos, pos);
        }

        // Does the guard see the given point (is faced towards it)
        public bool CanSpotPosition(Vector2 pos)
        {
            if (!CanSeePosition(pos))
            {
                return false;
            }

            Vector2 facing   = Vector2.Normalize(mFacing);
            Vector2 toTarget = Vector2.Normalize(pos - mPosition);
            var Dot = Vector2.Dot(facing, toTarget);

            if (Dot > Math.Cos(80.0 * Math.PI / 180.0))
            {
                return true;
            }

            return false;
        }

        // Checks whether the player can be spotted from the current position and rotation.
        // Similar to CanSeePlayer, but also calculates with the current rotation of the guard
        // and its field of view
        public bool CanSpotPlayer()
        {
            return CanSpotPosition(mMap.Player.GetPosition());
        }

        // =================== PRIVATE =================== //
        // Calculates which animations from animation sheet we need to use for rendering this guard
        void ChooseTextureIndices(out int aIndexX, out int aIndexY)
        {
            Vector2 ToPlayer = Vector2.Normalize(mMap.Player.GetPosition() - mPosition);
            Vector2 Facing = Vector2.Normalize(mFacing);
            float Dot = Vector2.Dot(ToPlayer, Facing);
            float Sign = Math.Sign(Vector3.Cross(new Vector3(ToPlayer.X, 0.0f, ToPlayer.Y), new Vector3(Facing.X, 0.0f, Facing.Y)).Y);

            aIndexX = 3;
            aIndexY = 0;
            int AnimationIndex = (int)mAnimationIndex % ANIM_INDICES_COUNT[(int)mCurrentAnimation];

            int Xdir;
            if (Dot > Math.Cos(22.5 * Math.PI / 180.0))     // front facing
            {
                Xdir = 0;
            }
            else if (Dot > Math.Cos((45.0 + 22.5) * Math.PI / 180.0))
            {
                if (Sign < 0)
                    Xdir = 1;
                else
                    Xdir = 7;
            }
            else if (Dot > Math.Cos((90.0 + 22.5) * Math.PI / 180.0))   // left / right
            {
                if (Sign < 0)
                    Xdir = 2;
                else
                    Xdir = 6;
            }
            else if (Dot > Math.Cos((135.0 + 22.5) * Math.PI / 180.0))
            {
                if (Sign < 0)
                    Xdir = 3;
                else
                    Xdir = 5;
            }
            else
            {
                Xdir = 4;
            }

            if (mCurrentAnimation == Animation.Still)
            {
                aIndexX = Xdir;
                aIndexY = 6;
            }
            else if (mCurrentAnimation == Animation.Walking)
            {
                aIndexX = Xdir;
                aIndexY = 5 - AnimationIndex;
            }
            else if (mCurrentAnimation == Animation.Death)
            {
                aIndexX = AnimationIndex;
                aIndexY = 1;
            }
            else if (mCurrentAnimation == Animation.Dead)
            {
                aIndexX = 4;
                aIndexY = 1;
            }
            else if (mCurrentAnimation == Animation.Attack)
            {
                aIndexX = AnimationIndex + 1;
                aIndexY = 0;
            }
            else if (mCurrentAnimation == Animation.Alert)
            {
                aIndexX = 0;
                aIndexY = 0;
            }
            else if (mCurrentAnimation == Animation.Hurt)
            {
                aIndexX = 7;
                aIndexY = 1;
            }
            else if (mCurrentAnimation == Animation.Aiming)
            {
                aIndexX = 1;
                aIndexY = 0;
            }
        }

        // Changes the current animation on the spot to some other one.
        // aHowLong is the minimal time the animation should remain active (even if it ends before that)
        void StartAnimation(Animation aAnimation, float aHowLong)
        {
            mCurrentAnimation = aAnimation;
            mAnimationCountdown = aHowLong;
            mAnimationIndex = 0;
        }

        // Continues moving on the current path if there is any
        void ContinuePath()
        {
            mMovingOnPath = true;
        }

        // Stops moving
        void StopMoving()
        {
            mMovingOnPath = false;
        }

        // Rotates towards the player
        void FacePlayer()
        {
            Vector2 PlayerLocation = mMap.Player.GetPosition();
            mFacing = Vector2.Normalize(PlayerLocation - mPosition);
        }

        // Alerts surrounding guards
        void Alert()
        {
            if (mTimeSinceLastAchtung > 1.0f)
            {
                Engine.GetSound().PlaySound(SoundType.EnemySpotted);
                Engine.GetAI().StreamEvent(AIEvent.Alert, GetPosition(), mMap);
                mTimeSinceLastAchtung = 0.0f;
            }
        }

        // Returns true if the current animation reached its final frame and the animation countdown
        // reached 0 as well
        bool AnimationEnded()
        {
            if (mAnimationIndex >= ANIM_INDICES_COUNT[(int)mCurrentAnimation] && mAnimationCountdown <= 0)
                return true;
            return false;
        }

        // Drains player's HP
        void HitPlayer()
        {
            float Distance = (mMap.Player.GetPosition() - mPosition).Length();
            int Damage = (int)(mDamage * Math.Max(1.0 - Distance / mRange, 0.0f));
            mMap.Player.Hurt(Damage);
        }
        
        // Updates the progress of character animation
        void UpdateAnimations(long aMs)
        {
            float Delta = aMs / (60.0f / 1000.0f);
            float AnimationSpeed = 0.0003f;
            mAnimationCountdown -= AnimationSpeed * Delta;

            if (mCurrentAnimation == Animation.Still)
            {
                mAnimationIndex = ANIM_INDICES_COUNT[(int)Animation.Still];
            }
            else if (mCurrentAnimation == Animation.Walking)
            {
                mAnimationIndex += AnimationSpeed * Delta;
            }
            else if (mCurrentAnimation == Animation.Attack)
            {
                float CurrentIndex = mAnimationIndex;
                float NextIndex = CurrentIndex + AnimationSpeed * Delta;
                if (CurrentIndex < 2.0f && NextIndex >= 2.0f)
                {
                    Engine.GetSound().PlaySound(SoundType.Pistol);
                    if (CanSeePlayer())
                    {
                        HitPlayer();
                    }
                }
                mAnimationIndex = NextIndex;
            }
            else if (mCurrentAnimation == Animation.Death)
            {
                mAnimationIndex += AnimationSpeed * Delta;
            }
            else if (mCurrentAnimation == Animation.Hurt)
            {
                mAnimationIndex = ANIM_INDICES_COUNT[(int)Animation.Hurt];
            }
            else if (mCurrentAnimation == Animation.Dead)
            {
                mAnimationIndex = ANIM_INDICES_COUNT[(int)Animation.Dead];
            }
            else if (mCurrentAnimation == Animation.Alert)
            {
                mAnimationIndex = ANIM_INDICES_COUNT[(int)Animation.Alert];
            }
            else if (mCurrentAnimation == Animation.Aiming)
            {
                mAnimationIndex = ANIM_INDICES_COUNT[(int)Animation.Aiming];
            }
        }

        // Updates the movement of the guard
        void UpdateMovement(long aMs)
        {
            if (mMovingOnPath)
            {
                if (mPath.Points.Count > 0)
                {
                    if (AnimationEnded() || mCurrentAnimation == Animation.Still)
                    {
                        StartAnimation(Animation.Walking, 0.0f);
                    }

                    if (!AnimationEnded() && mCurrentAnimation == Animation.Alert)
                    {
                        StartAnimation(Animation.Walking, 0.0f);
                    }

                    const float MovementSpeed = 0.0001f;
                    float Delta = aMs / (60.0f / 1000.0f);
                    Vector2 NextCorner = mPath.Points[0];
                    Vector2 F = NextCorner - mPosition;
                    Vector2 Direction = F.Length() < 0.001f ? new Vector2(1.0f, 0.0f) : Vector2.Normalize(F);
                    mFacing = Direction;
                    Vector2 MovementVector = Direction * MovementSpeed * Delta;

                    Vector2 NextPosition = mPosition + MovementVector;
                    int NextX = (int)NextPosition.X;
                    int NextY = (int)NextPosition.Y;

                    if (NextX >= mMap.CollisionGrid.GetLength(0)
                    || NextY  >= mMap.CollisionGrid.GetLength(1)
                    || mMap.CollisionGrid[NextX, NextY] == 1)
                    {
                        // if we are about to hit some wall
                        int ThisX = (int)mPosition.X;
                        int ThisY = (int)mPosition.Y;
                        mPosition = new Vector2(ThisX + 0.5f, ThisY + 0.5f);
                    }
                    else
                    {
                        if (Vector2.Distance(mPosition, NextCorner) > MovementVector.Length())
                        {
                            mPosition = NextPosition;
                        }
                        else
                        {
                            mPosition = NextCorner;
                            mPath.Points.RemoveAt(0);
                        }
                    }
                }
                else
                {
                    mMovingOnPath = false;
                }
            }
            else
            {
                if (mCurrentAnimation == Animation.Walking)
                {
                    StartAnimation(Animation.Still, 0.0f);
                }
            }
        }

        // Handles the wait plan step. Waits until a time runs out
        void HandlePlanState_Wait(float s)
        {
            if (mCurrentPlanStep.HasValue)
            {
                StartAnimation(Animation.Still, 0.0f);
                var step = mCurrentPlanStep.GetValueOrDefault();
                step.waitTime -= s;
                mCurrentPlanStep = step;
                if (step.waitTime < 0.0f)
                {
                    mCurrentPlanStep = null;
                }
                mCurrentPlanStep = step;
            }
        }

        // Handles the alert plan step. Alerts the surrounding guards
        void HandlePlanState_Alert(float s)
        {
            if (AnimationEnded() || mCurrentAnimation != Animation.Walking)
            {
                StartAnimation(Animation.Alert, 0.1f);
            }

            var step = mCurrentPlanStep.GetValueOrDefault();
            step.waitTime -= s;
            mCurrentPlanStep = step;

            if (step.waitTime < 0.0f)
            {
                Alert();
                mCurrentPlanStep = null;
            }
        }

        // Handles attack plan state. Attacks the player and moves around
        void HandlePlanState_Attack(float s)
        {
            if ((mCurrentAnimation == Animation.Attack || mCurrentAnimation == Animation.Aiming)
            && !AnimationEnded())
            {
                return;
            }

            var step = mCurrentPlanStep.GetValueOrDefault();
            if (step.attackCount <= 0)
            {
                mCurrentPlanStep = null;
                return;
            }

            StopMoving();
            FacePlayer();

            if (mCurrentAnimation == Animation.Alert || mCurrentAnimation == Animation.Aiming)
            {
                StartAnimation(Animation.Attack, 0.0f);
                step.attackCount -= 1;
            }
            else
            {
                if (mRand.Next(0, 100) < 50)
                {
                    StartAnimation(Animation.Attack, 0.0f);
                    step.attackCount -= 1;
                }
                else
                {
                    StartAnimation(Animation.Aiming, 3.0f);
                }
            }

            mCurrentPlanStep = step;
        }

        // Handles the moving plan step. Moves on the path until finished
        void HandlePlanState_Move(float s)
        {
            var step = mCurrentPlanStep.GetValueOrDefault();
            if (step.path == null || step.path.Points.Count == 0)
            {
                StopMoving();
                mCurrentPlanStep = null;
                return;
            }
            else
            {
                mPath = step.path;
                ContinuePath();
            }
        }

        // Updates the AI of the guard
        void UpdateAi(long aMs)
        {
            float s = aMs * 0.001f;
            mTimeSinceLastAchtung += s;

            // Do not update AI if the guard is dead
            if (mState == State.Dead)
            {
                StopMoving();
                if (AnimationEnded())
                {
                    const float DEATH_ANIM_LEN = 10000.0f;
                    StartAnimation(Animation.Dead, DEATH_ANIM_LEN);
                }
                return;
            }

            if (CanSpotPlayer())
            {
                mState = State.Alert;
                mLastSeenPlayerPos = mMap.Player.GetPosition();
                mTimeSinceLastSeenPlayer = 0.0f;
            }

            mTimeSinceLastSeenPlayer += s;
            if (mTimeSinceLastSeenPlayer > FORGET_TIME)
            {
                mState = State.Still;
            }

            // Do nothing if the guard is stuck in hurt animation
            if (mCurrentAnimation == Animation.Hurt && !AnimationEnded())
            {
                return;
            }

            if (mCurrentPlanStep == null && mPlan?.steps?.Count == 0)
            {
                mPlan = null;
            }

            var aiSys = Engine.GetAI();
            if (!aiSys.IsPlanValid(mPlan))
            {
                var newPlan = aiSys.MakePlan(this, mMap, mPlan);
                SetPlan(newPlan);
            }

            // follow the new plan
            if (mPlan != null && (mPlan.steps.Count > 0 || mCurrentPlanStep != null))
            {
                if (mCurrentPlanStep == null)
                {
                    mCurrentPlanStep = mPlan.steps[0];
                    mPlan.steps.RemoveAt(0);
                }

                switch (mCurrentPlanStep?.type)
                {
                    case AIPlan.StepType.Attack:
                    {
                        HandlePlanState_Attack(s);
                        break;
                    }

                    case AIPlan.StepType.Move:
                    {
                        HandlePlanState_Move(s);
                        break;
                    }

                    case AIPlan.StepType.Wait:
                    {
                        HandlePlanState_Wait(s);
                        break;
                    }

                    case AIPlan.StepType.Alert:
                    {
                        HandlePlanState_Alert(s);
                        break;
                    }
                }
            }
        }
    }
}
