﻿using System.Collections.Generic;
using static OpenGL.GL;

namespace Wolf
{
    class Geometry
    {
        public static uint WallVertexBufferHandle       { get; private set; }
        public static uint HorizontalDoorVertexBuffer   { get; private set; }
        public static uint VerticalDoorVertexBuffer     { get; private set; }
        public static uint DoorIndexBuffer              { get; private set; }
        public static int  DoorIndicesCount             { get; private set; }
        public static uint BillboardVertexBufferHandle  { get; private set; }
        public static uint[,,,] WallIndexBufferHandle   { get; private set; }
        public static uint BillboardIndexBufferHandle   { get; private set; }
        public static int [,,,] WallIndicesCount        { get; private set; }
        public static int  BillboardIndicesCount        { get; private set; }
        public static uint WallVertexArrayHandle        { get; private set; }
        public static uint BillboardVertexArrayHandle   { get; private set; }

        // Generates the geometry required for rendering
        // It is then possible to get it by the getters above
        public static void GenerateGeometry()
        {
            GenerateVertexBuffers();
            GenerateIndexBuffers();
        }

        // Cleans up the mess after ourselves
        public static void CleanUp()
        {
            glDeleteBuffer(WallVertexBufferHandle);
            glDeleteBuffer(BillboardVertexBufferHandle);

            for (int s = 0; s <= 1; s++)
            {
                for (int w = 0; w <= 1; w++)
                {
                    for (int n = 0; n <= 1; n++)
                    {
                        for (int e = 0; e <= 1; e++)
                        {
                            uint Handle = WallIndexBufferHandle[s, w, n, e];
                            glDeleteBuffer(Handle);
                        }
                    }
                }
            }
            glDeleteBuffer(BillboardIndexBufferHandle);

            glDeleteVertexArray(WallVertexArrayHandle);
            glDeleteVertexArray(BillboardVertexArrayHandle);
        }

        static void GenerateVertexBuffers()
        {
            float[] WallVertices = new float[]
            {
                // south
                 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
                 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
                 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
                 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,

                // west
                 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
                 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
                 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
                 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,

                // north
                 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 2.0f,
                 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 2.0f,
                 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 2.0f,
                 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 2.0f,

                 // east
                 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 3.0f,
                 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 3.0f,
                 1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 3.0f,
                 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 3.0f
            };

            WallVertexArrayHandle = glGenVertexArray();
            glBindVertexArray(WallVertexArrayHandle);

            WallVertexBufferHandle = glGenBuffer();
            glBindBuffer(GL_ARRAY_BUFFER, WallVertexBufferHandle);

            unsafe
            {
                fixed (float* Data = &WallVertices[0])
                {
                    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * WallVertices.Length, Data, GL_STATIC_DRAW);
                }
            }

            float[] HorizontalDoorVertices = new float[]
            {
                // south
                0.0f, 0.0f, 0.525f, 0.0f, 0.0f, 0.0f,
                1.0f, 0.0f, 0.525f, 1.0f, 0.0f, 0.0f,
                1.0f, 1.0f, 0.525f, 1.0f, 1.0f, 0.0f,
                0.0f, 1.0f, 0.525f, 0.0f, 1.0f, 0.0f,

                // north
                1.0f, 0.0f, 0.475f, 1.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 0.475f, 0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.475f, 0.0f, 1.0f, 0.0f,
                1.0f, 1.0f, 0.475f, 1.0f, 1.0f, 0.0f
            };

            float[] VerticalDoorVertices = new float[]
            {
                // west
                0.475f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
                0.475f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
                0.475f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
                0.475f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

                // east
                0.525f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
                0.525f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
                0.525f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
                0.525f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f
            };

            HorizontalDoorVertexBuffer = glGenBuffer();
            glBindBuffer(GL_ARRAY_BUFFER, HorizontalDoorVertexBuffer);

            unsafe
            {
                fixed (float* Data = &HorizontalDoorVertices[0])
                {
                    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * HorizontalDoorVertices.Length, Data, GL_STATIC_DRAW);
                }
            }

            VerticalDoorVertexBuffer = glGenBuffer();
            glBindBuffer(GL_ARRAY_BUFFER, VerticalDoorVertexBuffer);

            unsafe
            {
                fixed (float* Data = &VerticalDoorVertices[0])
                {
                    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * VerticalDoorVertices.Length, Data, GL_STATIC_DRAW);
                }
            }

            float[] BillboardVertices = new float[]
            {
               -0.5f, 0.0f, 0.0f, 0.0f, 0.0f,
                0.5f, 0.0f, 0.0f, 1.0f, 0.0f,
                0.5f, 1.0f, 0.0f, 1.0f, 1.0f,
               -0.5f, 1.0f, 0.0f, 0.0f, 1.0f
            };

            BillboardVertexArrayHandle = glGenVertexArray();
            glBindVertexArray(BillboardVertexArrayHandle);

            BillboardVertexBufferHandle = glGenBuffer();
            glBindBuffer(GL_ARRAY_BUFFER, BillboardVertexBufferHandle);

            unsafe
            {
                fixed (float* Data = &BillboardVertices[0])
                {
                    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * BillboardVertices.Length, Data, GL_STATIC_DRAW);
                }

                glVertexAttribPointer(0, 3, GL_FLOAT, false, 5 * sizeof(float), NULL);
                glEnableVertexAttribArray(0);
                glVertexAttribPointer(1, 2, GL_FLOAT, false, 5 * sizeof(float), (void*)(3 * sizeof(float)));
                glEnableVertexAttribArray(1);
            }

            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }

        static void GenerateIndexBuffers()
        {
            WallIndexBufferHandle = new uint[2, 2, 2, 2];   // from left: south, west, north, east
            WallIndicesCount = new int[2, 2, 2, 2];

            List<uint> WallIndicesS = new List<uint>
            {
                0, 1, 2,
                0, 2, 3
            };

            List<uint> WallIndicesW = new List<uint>
            {
                4, 5, 6,
                4, 6, 7
            };

            List<uint> WallIndicesN = new List<uint>
            {
                8, 9, 10,
                8, 10, 11
            };

            List<uint> WallIndicesE = new List<uint>
            {
                12, 13, 14,
                12, 14, 15
            };

            for (int s = 0; s <= 1; s++)
            {
                for (int w = 0; w <= 1; w++)
                {
                    for (int n = 0; n <= 1; n++)
                    {
                        for (int e = 0; e <= 1; e++)
                        {
                            int Sum = s + w + n + e;

                            if (Sum != 0)
                            {
                                List<uint> Indices = new List<uint>();
                                if (s == 1)
                                    Indices.AddRange(WallIndicesS);
                                if (w == 1)
                                    Indices.AddRange(WallIndicesW);
                                if (n == 1)
                                    Indices.AddRange(WallIndicesN);
                                if (e == 1)
                                    Indices.AddRange(WallIndicesE);

                                uint[] RawData = Indices.ToArray();
                                uint BufferHandle = glGenBuffer();
                                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, BufferHandle);

                                unsafe
                                {
                                    fixed (uint* Data = RawData)
                                    {
                                        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * Indices.Count, Data, GL_STATIC_DRAW);
                                    }
                                }

                                WallIndexBufferHandle[s, w, n, e] = BufferHandle;
                                WallIndicesCount[s, w, n, e] = Indices.Count;
                            }
                        }
                    }
                }
            }

            uint[] BillboardIndices = new uint[]
            {
                0, 1, 2,
                0, 2, 3
            };

            BillboardIndexBufferHandle = glGenBuffer();
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, BillboardIndexBufferHandle);
            BillboardIndicesCount = BillboardIndices.Length;

            unsafe
            {
                fixed (uint* Data = BillboardIndices)
                {
                    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * BillboardIndices.Length, Data, GL_STATIC_DRAW);
                }
            }

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

            uint[] DoorIndices = new uint[]
            {
                0, 1, 2,
                0, 2, 3,

                4, 5, 6,
                4, 6, 7
            };

            DoorIndexBuffer = glGenBuffer();
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, DoorIndexBuffer);
            DoorIndicesCount = DoorIndices.Length;

            unsafe
            {
                fixed (uint* Data = DoorIndices)
                {
                    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * DoorIndices.Length, Data, GL_STATIC_DRAW);
                }
            }

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        }
    }
}
