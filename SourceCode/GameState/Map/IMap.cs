﻿using System;
using System.Collections.Generic;
using System.Numerics;
using static OpenGL.GL;
using BigGustave;

namespace Wolf
{
    public abstract class IMap
    {
        // ====== OBJECTS OF THE MAP ====== //
        public List<IRenderable>      Walls           { get; protected set; }
        public List<IRenderable>      Doors           { get; protected set; }
        public List<IRenderable>      Billboards      { get; protected set; }
        public List<IRenderable>      Pickups         { get; protected set; }
        public Vector3          FloorColor      { get; protected set; }
        public Vector3          CeilingColor    { get; protected set; }
        public Player           Player          { get; protected set; }
        public List<IActive>    Active          { get; protected set; }
        public List<IUsable>    Usable          { get; protected set; }
        public List<Guard>      Enemies         { get; protected set; }
        public int[,]           CollisionGrid   { get; protected set; }
        public uint[]           GunTextures     { get; protected set; }
        public uint[]           HotbarTextures  { get; protected set; }
        public uint             AmmoTexture     { get; protected set; }

        public Vector2   PlayerStartingPosition { get; protected set; }
        public Vector2   PlayerStartingFacing   { get; protected set; }

        // Has player finished this level?
        public bool Finished { get; set; }

        // Was the map already loaded?
        public bool mLoaded                     { get; protected set; }

        protected string      mPath;            // directory in which the resources for the level are located
        protected uint        mLoadingScreen;   // handle to the loading screen texture
        protected List<uint>  mTexturesOnGpu;   // all textures allocated on gpu during this level loading

        // Debugging texture for paths of the guards
        protected uint mDebugPathsTexture;

        static readonly string LOADING_SCREEN_IMAGE = "loading.png";

        // ============== PUBLIC ============== //

        // Override this for custom map loading
        abstract public void LoadMap();

        // Returns a handle to the texture that should be displayed when loading the level
        public uint GetLoadingTexture()
        {
            return mLoadingScreen;
        }

        // Checks if there are some walls or doors between two points
        public bool CanHit(Vector2 aFrom, Vector2 aTo)
        {
            return RayCast(aFrom, aTo, false);
        }

        // Checks if there are some walls between two points
        public bool CanSee(Vector2 aFrom, Vector2 aTo)
        {
            return RayCast(aFrom, aTo, true);
        }

        // Creates an attack path for the guard. All points on this path are such that player can be seen and hit from them
        public Path CreateAttackPath(Vector2 aFrom, int aLength)
        {
            // We are searching for attack path.
            // Attack path:
            //  From every node of the path we must be able to see player - so we can hit him easily
            //  Path from our location to this node must not exceed 'aLength' distance
            // With BFS we find set of viable nodes and then randomly pick one of them
            var VisitedNodes    = new HashSet<Tuple<int, int>>();
            var PreviousNode    = new Dictionary<Tuple<int, int>, Tuple<int, int>>();
            var NodeLevel       = new Dictionary<Tuple<int, int>, int>();
            var NodesToVisit    = new List<Tuple<int, int, int>>();   // xcoord, ycoord & level
            var GoodNodes       = new List<Tuple<int, int>>();
            int FromX = (int)aFrom.X;
            int FromY = (int)aFrom.Y;
            var Pair  = Tuple.Create(FromX, FromY);
            var Trio  = Tuple.Create(FromX, FromY, 0);

            NodesToVisit.Add(Trio); // node x, node y & node level
            VisitedNodes.Add(Pair);
            NodeLevel.Add(Pair, 0);

            int[] OffsetsX = new int[] { 0, -1, 1, 0 };
            int[] OffsetsY = new int[] { -1, 0, 0, 1 };

            while (NodesToVisit.Count > 0)
            {
                Tuple<int, int, int> CurrentNode = NodesToVisit[0];
                NodesToVisit.RemoveAt(0);

                int GridX = CurrentNode.Item1;
                int GridY = CurrentNode.Item2;
                int Level = CurrentNode.Item3;
                Tuple<int, int> CurrentPair = Tuple.Create(GridX, GridY);

                if (Level >= aLength)
                {
                    GoodNodes.Add(CurrentPair);
                }
                else
                {
                    for (int i = 0; i < OffsetsX.Length; i++) // search all neightbour nodes
                    {
                        int XCoord = GridX + OffsetsX[i];
                        int YCoord = GridY + OffsetsY[i];
                        var NewPair = Tuple.Create(XCoord, YCoord);
                        var NewTrio = Tuple.Create(XCoord, YCoord, Level + 1);

                        if (XCoord >= 0 && XCoord < CollisionGrid.GetLength(0) &&
                            YCoord >= 0 && YCoord < CollisionGrid.GetLength(1) &&
                            !VisitedNodes.Contains(NewPair))
                        {
                            Vector2 PlayerPos = new Vector2((int)Player.GetPosition().X, (int)Player.GetPosition().Y) + new Vector2(0.5f);
                            Vector2 BlockPos = new Vector2(XCoord, YCoord) + new Vector2(0.5f);
                            if (CollisionGrid[XCoord, YCoord] != 1 && CanSee(BlockPos, PlayerPos))        // only if we can shoot on player from this location
                            {
                                NodesToVisit.Add(NewTrio);
                                PreviousNode.Add(NewPair, CurrentPair);
                                VisitedNodes.Add(NewPair);
                            }
                        }
                    }
                }
            }

            if (GoodNodes.Count == 0)
            {
                return null;
            }

            // now choose one of good nodes
            var Rand   = new Random();
            int Choice = Rand.Next(0, GoodNodes.Count);
            var GoodPair = GoodNodes[Choice];
            var Path     = new List<Vector2>();
            while (PreviousNode.ContainsKey(GoodPair))
            {
                var Position = new Vector2(GoodPair.Item1, GoodPair.Item2) + new Vector2(0.5f);
                GoodPair = PreviousNode[GoodPair];
                Path.Add(Position);
            }

            Path NewPath = new Path();
            NewPath.Points = Path;
            return NewPath;
        }
            
        // Creates a shortest path from aFrom to aTo.
        // aLength is the maximum distance of the path
        // Might return null if no path is found or it is too long
        public Path CreateFollowPath(Vector2 aFrom, Vector2 aTo, int aLength)
        {
            var VisitedNodes = new HashSet<Tuple<int, int>>();
            var PreviousNode = new Dictionary<Tuple<int, int>, Tuple<int, int>>();
            var NodeLevel    = new Dictionary<Tuple<int, int>, int>();
            var NodesToVisit = new List<Tuple<int, int, int>>();   // xcoord, ycoord & level
            Tuple<int, int> MinNode = null;

            int FromX = (int)aFrom.X;
            int FromY = (int)aFrom.Y;
            int ToX = (int)aTo.X;
            int ToY = (int)aTo.Y;

            var Pair = Tuple.Create(FromX, FromY);
            var Trio = Tuple.Create(FromX, FromY, 0);

            NodesToVisit.Add(Trio); // node x, node y & node level
            VisitedNodes.Add(Pair);
            NodeLevel.Add(Pair, 0);

            int[] OffsetsX = new int[] { 0, -1, 1, 0 };
            int[] OffsetsY = new int[] { -1, 0, 0, 1 };
            Tuple<int, int> TargetFound = null;
            float MinDistance = Vector2.Distance(aFrom, aTo);

            while (NodesToVisit.Count > 0)
            {
                Tuple<int, int, int> CurrentNode = NodesToVisit[0];
                NodesToVisit.RemoveAt(0);

                int GridX = CurrentNode.Item1;
                int GridY = CurrentNode.Item2;
                int Level = CurrentNode.Item3;
                var CurrentPair = Tuple.Create(GridX, GridY);

                if (GridX == ToX && GridY == ToY)
                {
                    TargetFound = Tuple.Create(GridX, GridY);
                    break;
                }

                float Distance = Vector2.Distance(aTo, new Vector2(GridX, GridY) + new Vector2(0.5f));
                if (Distance < MinDistance)
                {
                    MinNode = Tuple.Create(GridX, GridY);
                    MinDistance = Distance;
                }

                if (Level < aLength)
                {
                    for (int i = 0; i < OffsetsX.Length; i++) // search all neightbour nodes
                    {
                        int XCoord = GridX + OffsetsX[i];
                        int YCoord = GridY + OffsetsY[i];
                        var NewPair = Tuple.Create(XCoord, YCoord);
                        var NewTrio = Tuple.Create(XCoord, YCoord, Level + 1);

                        if (XCoord >= 0 && XCoord < CollisionGrid.GetLength(0) &&
                            YCoord >= 0 && YCoord < CollisionGrid.GetLength(1) &&
                            !VisitedNodes.Contains(NewPair))
                        {
                            if (CollisionGrid[XCoord, YCoord] != 1)
                            {
                                NodesToVisit.Add(NewTrio);
                                PreviousNode.Add(NewPair, CurrentPair);
                                VisitedNodes.Add(NewPair);
                            }
                        }
                    }
                }
            }

            Tuple<int, int> GoodPair;
            if (TargetFound == null)
            {
                if (MinNode == null)
                {
                    return null;
                }

                GoodPair = MinNode;
            }
            else
            {
                GoodPair = TargetFound;
            }
            
            var Path = new List<Vector2>();
            while (PreviousNode.ContainsKey(GoodPair))
            {
                Vector2 Position = new Vector2(GoodPair.Item1, GoodPair.Item2) + new Vector2(0.5f);
                GoodPair = PreviousNode[GoodPair];
                Path.Add(Position);
            }

            Path.Reverse();

            Path NewPath = new Path();
            NewPath.Points = Path;
            return NewPath;
        }

        public IMap(string mPathPath)
        {
            mPath = Engine.RESOURCES_DIR + mPathPath + "/";
            Finished = false;

            Pickups         = new List<IRenderable>();
            GunTextures     = new uint[4];
            HotbarTextures  = new uint[4];
            Walls           = new List<IRenderable>();
            Doors           = new List<IRenderable>();
            Billboards      = new List<IRenderable>();
            Active          = new List<IActive>();
            Usable          = new List<IUsable>();
            Enemies         = new List<Guard>();

            var LoadingScreen = Png.Open(mPath + LOADING_SCREEN_IMAGE);
            mLoadingScreen = LoadTextureFromPngToGpu(LoadingScreen, Vector2.Zero, new Vector2(LoadingScreen.Width, LoadingScreen.Height));
            mLoaded = false;
        }

        // Cleans up all resources allocated by the level
        public void Cleanup()
        {
            // cleanup GPU
            foreach (uint Tex in mTexturesOnGpu)
            {
                unsafe
                {
                    glDeleteTexture(Tex);
                }
            }
        }

        // =================== PRIVATE =================== //
        // Casts a line segment and checks if it collides with something
        // if aOnly walls is true it checks only for collisions with walls
        // in other case checks also for collisions with doors
        bool RayCast(Vector2 aFrom, Vector2 aTo, bool aOnlyWalls)
        {
            Vector2 FromTo = aTo - aFrom;

            int FromGridX = (int)aFrom.X;
            int ToGridX   = (int)aTo.X;

            int Direction = FromGridX < ToGridX ? 1 : -1;
            for (int i = FromGridX; i != ToGridX; i += Direction)
            {
                float VerticalLine = i;
                if (Direction == 1)
                    VerticalLine += 1;

                float Diff = VerticalLine - aFrom.X;
                float Mul = Diff / FromTo.X;
                Vector2 Intersection = aFrom + FromTo * Mul;

                if ((int)Intersection.X >= 0 &&
                    (int)Intersection.X < CollisionGrid.GetLength(0) &&
                    (int)Intersection.Y >= 0 &&
                    (int)Intersection.Y < CollisionGrid.GetLength(1) &&
                    !((int)aFrom.X == (int)Intersection.X &&
                    (int)aFrom.Y == (int)Intersection.Y))
                {
                    if (GridIntersect((int)Intersection.X, (int)Intersection.Y, aOnlyWalls))
                    {
                        return false;
                    }
                }

                if ((int)Intersection.X - 1 >= 0 &&
                    (int)Intersection.X - 1 < CollisionGrid.GetLength(0) &&
                    (int)Intersection.Y >= 0 &&
                    (int)Intersection.Y < CollisionGrid.GetLength(1) &&
                    !((int)aFrom.X == (int)Intersection.X - 1 &&
                    (int)aFrom.Y == (int)Intersection.Y))
                {
                    if (GridIntersect((int)Intersection.X - 1, (int)Intersection.Y, aOnlyWalls))
                    {
                        return false;
                    }
                }
            }

            int FromGridY = (int)aFrom.Y;
            int ToGridY = (int)aTo.Y;

            Direction = FromGridY < ToGridY ? 1 : -1;
            for (int i = FromGridY; i != ToGridY; i += Direction)
            {
                float HorizontalLine = i;
                if (Direction == 1)
                    HorizontalLine += 1;

                float Diff = HorizontalLine - aFrom.Y;
                float Mul = Diff / FromTo.Y;
                Vector2 Intersection = aFrom + FromTo * Mul;

                if ((int)Intersection.X >= 0 &&
                    (int)Intersection.X < CollisionGrid.GetLength(0) &&
                    (int)Intersection.Y >= 0 &&
                    (int)Intersection.Y < CollisionGrid.GetLength(1) &&
                    !((int)aFrom.X == (int)Intersection.X &&
                    (int)aFrom.Y == (int)Intersection.Y))
                {
                    if (GridIntersect((int)Intersection.X, (int)Intersection.Y, aOnlyWalls))
                    {
                        return false;
                    }
                }


                if ((int)Intersection.X >= 0 &&
                    (int)Intersection.X < CollisionGrid.GetLength(0) &&
                    (int)Intersection.Y - 1 >= 0 &&
                    (int)Intersection.Y - 1 < CollisionGrid.GetLength(1) &&
                    !((int)aFrom.X == (int)Intersection.X &&
                    (int)aFrom.Y == (int)Intersection.Y - 1))
                {
                    if (GridIntersect((int)Intersection.X, (int)Intersection.Y - 1, aOnlyWalls))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        // Checks if there is some object on this grid position
        // If onlyWalls is true then only walls are checked.
        // Checks for the door as well in the other case
        bool GridIntersect(int x, int y, bool onlyWalls)
        {
            if (x < 0 || x >= CollisionGrid.GetLength(0))
            {
                return false;
            }

            if (y < 0 || y >= CollisionGrid.GetLength(1))
            {
                return false;
            }

            if (onlyWalls)
            {
                if (CollisionGrid[x, y] == 1)
                    return true;
                else
                    return false;
            }
            else
            {
                if (CollisionGrid[x, y] != 0)
                    return true;
                else
                    return false;
            }
        }

        // Loads part of the texture from Png image into the GPU.
        // All allocated textures are freed by callign `Cleanup`.
        protected uint LoadTextureFromPngToGpu(Png aImage, Vector2 aFrom, Vector2 aTo, bool aAlpha = false)
        {
            if (mTexturesOnGpu == null)
                mTexturesOnGpu = new List<uint>();

            if (aImage == null)
            {
                throw new ArgumentNullException();
            }

            if (aTo.X > aImage.Width || aTo.Y > aImage.Height || aFrom.X >= aTo.X || aFrom.Y >= aTo.Y)
            {
                throw new Exception("Invalid texture range");
            }

            int TextureWidth  = (int)(aTo.X - aFrom.X);
            int TextureHeight = (int)(aTo.Y - aFrom.Y);

            int Channels = aAlpha ? 4 : 3;
            byte[] ImageData = new byte[TextureWidth * TextureHeight * Channels];
            int Index = (TextureWidth * TextureHeight * Channels);
            for (int y = (int)aFrom.Y; y < (int)aTo.Y; y++)
            {
                for (int x = (int)aTo.X - 1; x >= (int)aFrom.X; x--)
                {
                    Pixel CurrentPixel = aImage.GetPixel(x, y);
                    if (aAlpha)
                    {
                        ImageData[Index - 4] = CurrentPixel.R;
                        ImageData[Index - 3] = CurrentPixel.G;
                        ImageData[Index - 2] = CurrentPixel.B;
                        ImageData[Index - 1] = CurrentPixel.A;
                    }
                    else
                    {
                        ImageData[Index - 3] = CurrentPixel.R;
                        ImageData[Index - 2] = CurrentPixel.G;
                        ImageData[Index - 1] = CurrentPixel.B;
                    }
                        
                    Index -= Channels;
                }
            }

            uint TextureHandle;

            unsafe
            {
                TextureHandle = glGenTexture();
                glBindTexture(GL_TEXTURE_2D, TextureHandle);
                int Format = aAlpha ? GL_RGBA : GL_RGB;

                fixed (void* Data = &ImageData[0])
                {
                    glTexImage2D(GL_TEXTURE_2D, 0, Format, TextureWidth, TextureHeight, 0, Format, GL_UNSIGNED_BYTE, Data);
                    glGenerateMipmap(GL_TEXTURE_2D);

                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
                }
            }

            mTexturesOnGpu.Add(TextureHandle);

            return TextureHandle;
        }

        protected void PickIboForWall(Png aMapImage, int aX, int aY, List<Vector3> aWallColors, out uint aIbo, out int aCount)
        {
            Pixel Below = new Pixel(0, 0, 0, 0, false);
            Pixel Left =  new Pixel(0, 0, 0, 0, false);
            Pixel Above = new Pixel(0, 0, 0, 0, false);
            Pixel Right = new Pixel(0, 0, 0, 0, false);

            if (aY + 1 < aMapImage.Height)
                Below = aMapImage.GetPixel(aX, aY + 1);
            if (aX + 1 < aMapImage.Width)
                Right = aMapImage.GetPixel(aX + 1, aY);
            if (aY - 1 >= 0)
                Above = aMapImage.GetPixel(aX, aY - 1);
            if (aX - 1 >= 0)
                Left = aMapImage.GetPixel(aX - 1, aY);

            Vector3 BelowVec = Below.ToVec3();
            Vector3 LeftVec =  Left.ToVec3();
            Vector3 AboveVec = Above.ToVec3();
            Vector3 RightVec = Right.ToVec3();

            int S = 1, W = 1, N = 1, E = 1;

            if (aWallColors != null)
            {
                if (aWallColors.Contains(BelowVec))
                    S = 0;
                if (aWallColors.Contains(AboveVec))
                    N = 0;
                if (aWallColors.Contains(LeftVec))
                    W = 0;
                if (aWallColors.Contains(RightVec))
                    E = 0;
            }

            aIbo   = Geometry.WallIndexBufferHandle[S, W, N, E];
            aCount = Geometry.WallIndicesCount[S, W, N, E];
        }
    }
}
