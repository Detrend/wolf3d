using System;
using System.IO;
using System.Text.Json;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Wolf
{
    public sealed partial class Engine
    {
        // Initializes the sound system
        public void InitSoundSystem()
        {
            mSoundSystem = new SoundSystem();
            mSoundSystem.Init();
        }

        // Shuts down the sound system
        public void ShutDownSoundSystem()
        {
            mSoundSystem.ShutDown();
            mSoundSystem = null;
        }

        // Returns the sound system
        public static SoundSystem GetSound()
        {
            return Engine.GetEngine().mSoundSystem;
        }

        SoundSystem mSoundSystem;
    }

    public enum SoundType
    {
        // weapons
        Knife = 0,
        Pistol,
        Smg,
        Machinegun,

        // player
        PlayerPain,
        PlayerDeath,

        // pickups
        Ammo,
        Pickup,
        WeaponPickup,
        Heal,

        // objects
        Door,
        Secret,

        // enemies
        EnemySpotted,
        EnemyPain,
        EnemyDeath,
    }

    //////////////////
    // SOUND SYSTEM //
    //////////////////
    public sealed class SoundSystem
    {
        public readonly string SoundDirectory = "Sounds/";
        public readonly string SoundBindings  = "Sounds/sound_bindings.json";

        // Turns the sounds off
        // just for debugging purposes
        public static readonly bool ENABLED = true;

        // Initializes the sound system
        public void Init()
        {
            mSoundQueue = new List<string>();
            mSoundQueueLock = new Mutex();

            BuildSFXDictionary();

            if (ENABLED)
            {
                mSoundPlayWorker = new Thread(new ParameterizedThreadStart(SoundPlayer));
                mSoundPlayWorker.Start(this);
            }
        }

        void BuildSFXDictionary()
        {
            // deserialize sound paths
            byte[] content;

            try
            {
                content = File.ReadAllBytes(Engine.RESOURCES_DIR + SoundBindings);
            }
            catch
            {
                Console.WriteLine("Failed to open file with sound bindings.");
                throw;
            }

            try
            {
                Dictionary<string, List<string>> dictionary;
                dictionary = JsonSerializer.Deserialize<Dictionary<string, List<string>>>(content);
                mSoundDict = new Dictionary<SoundType, List<string>>();

                foreach (var node in dictionary)
                {
                    var name = node.Key;
                    var list = node.Value;
                    var type = (SoundType)Enum.Parse(typeof(SoundType), name);
                    mSoundDict.Add(type, list);
                }
            }
            catch
            {
                Console.WriteLine("Failed to deserialize sound bindings.");
                throw;
            }
        }

        // Shuts down the sound system
        public void ShutDown()
        {
            mShouldEnd = true;
            if (ENABLED)
            {
                mSoundPlayWorker.Join();
            }
        }

        // Check for the worker thread whether it should stop
        public bool ShouldEnd()
        {
            return mShouldEnd;
        }

        // These 2 let us play sounds
        [DllImport("winmm.dll")]
        private static extern int mciSendString(string command, StringBuilder stringReturn, int returnLength, IntPtr hwndCallback);
        [DllImport("winmm.dll")]
        private static extern int mciGetErrorString(int errorCode, StringBuilder errorText, int errorTextSize);

        // Plays a sound. This function blocks. Call it from a separate thread
        public static void PlaySoundFromFileBlocking(string soundToPlay)
        {
            // we need to create a separate track for each new sound so multiple sounds
            // can play at the same time
            string trackName = "track_" + sPlayedSoundCounter.ToString();
            sPlayedSoundCounter += 1;

            const int ERROR_MSG_LEN = 256;

            var retval = mciSendString("open \"" + soundToPlay + "\" alias " + trackName, null, 0, IntPtr.Zero);
            if (retval != 0)
            {
                StringBuilder errorStr = new StringBuilder(ERROR_MSG_LEN);
                mciGetErrorString(retval, errorStr, ERROR_MSG_LEN);
                Console.WriteLine("Sound system error: \"{0}\"", errorStr);
            }

            retval = mciSendString("Play " + trackName, null, 0, IntPtr.Zero);
            if (retval != 0)
            {
                StringBuilder errorStr = new StringBuilder(ERROR_MSG_LEN);
                mciGetErrorString(retval, errorStr, ERROR_MSG_LEN);
                Console.WriteLine("Sound system error: \"{0}\"", errorStr);
            }
        }

        // We need a separate thread for playing sounds because the windows function 
        // `mciSendString` which we use for audio is blocking. We do not want to block the
        // main thread, but we can afford to block second thread.
        // This function runs on the second thread and checks every 2ms if there is some sound in the
        // queue that should be played. If so, then picks it up and plays it.
        static void SoundPlayer(object soundSystemRef)
        {
            SoundSystem ss = soundSystemRef as SoundSystem;
            while (true)
            {
                string soundToPlay = null;

                // lets wait for some in the queue
                while (soundToPlay == null)
                {
                    // exit if the thread should end
                    if (ss.ShouldEnd())
                    {
                        return;
                    }

                    Thread.Sleep(2); // wait some time so that we do not lock the mutex too often
                    soundToPlay = ss.PopQueue();
                }

                PlaySoundFromFileBlocking(soundToPlay);
            }
        }

        // Pops the sound queue. Thread safe
        string PopQueue()
        {
            string bottom = null;

            mSoundQueueLock.WaitOne();
            if (mSoundQueue.Count > 0)
            {
                bottom = mSoundQueue[0];
                mSoundQueue.RemoveAt(0);
            }
            mSoundQueueLock.ReleaseMutex();

            return bottom;
        }

        // Pushes a new sounds effect into the queue
        public void PlaySoundFromFile(string relativeSoundPath)
        {
            var soundPathWithinAssets = Engine.RESOURCES_DIR + SoundDirectory + "/" + relativeSoundPath;
            if (!File.Exists(soundPathWithinAssets))
            {
                Console.WriteLine("Sound file \"{0}\" does not exist.", soundPathWithinAssets);
                return;
            }

            mSoundQueueLock.WaitOne();
            mSoundQueue.Add(soundPathWithinAssets);
            mSoundQueueLock.ReleaseMutex();
        }

        // Plays a loaded sound from the sound dictionary
        public void PlaySound(SoundType type)
        {
            if (!mSoundDict.ContainsKey(type))
            {
                Console.WriteLine(
                    "No sound files associated with sound type \"{0}\"",
                    Enum.GetName(typeof(SoundType), type));
                return;
            }

            var soundList = mSoundDict[type];
            if (soundList.Count == 0)
            {
                Console.WriteLine(
                    "No sounds associated with sound type \"{0}\".",
                    Enum.GetName(typeof(SoundType), type));
                return;
            }

            int index = mSoundRandomGenerator.Next(0, soundList.Count);
            var soundName = soundList[index];

            PlaySoundFromFile(soundName);
        }

        static long sPlayedSoundCounter = 0;

        List<string> mSoundQueue;
        Mutex        mSoundQueueLock;
        bool         mShouldEnd = false;
        Thread       mSoundPlayWorker;

        Dictionary<SoundType, List<string>> mSoundDict;
        Random mSoundRandomGenerator = new Random();
    }
}

