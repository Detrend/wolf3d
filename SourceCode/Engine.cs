﻿using GLFW;
using System.Diagnostics;

namespace Wolf
{
    // Engine class for everything
    public sealed partial class Engine
    {
        // directory for the game resources
        public static readonly string RESOURCES_DIR = "Assets/";

        // Returns the engine singleton
        public static Engine GetEngine()
        {
            return mEngineSingleton;
        }

        // Initializes the engine
        public void Init()
        {
            if (mEngineSingleton != null)
            {
                throw new System.Exception("Two engines living at the same time.");
            }
            mEngineSingleton = this;

            InitGraphics();
            InitInputSystem(GetGraphics().GetWindow());
            InitSceneSystem();
            InitSoundSystem();
            InitAiSystem();
        }

        // Runs the game
        // This function returns when the game ends
        public void Run()
        {
            Stopwatch Timer = new Stopwatch();
            Timer.Start();
            long PreviousFrameTime = Timer.ElapsedMilliseconds;

            while (!Glfw.WindowShouldClose(GetGraphics().GetWindow()))
            {
                try
                {
                    long TimeDelta, CurrentTime;
                    do
                    {
                        CurrentTime = Timer.ElapsedMilliseconds;
                        TimeDelta   = CurrentTime - PreviousFrameTime;
                    } while (mFpsCap && TimeDelta < 1000 / 60); ;
                    PreviousFrameTime = CurrentTime;

                    float passedSeconds = TimeDelta / 1000.0f;

                    GetInputSystem().Update(passedSeconds);
                    UpdateSceneSystem(passedSeconds);
                    GetGraphics().Update(passedSeconds, GetScene());
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    Exit();
                    throw;
                }
            }
        }

        // Terminates the engine and all allocated resources and ends the game
        public void Exit()
        {
            ShutDownAiSystem();
            ShutDownSoundSystem();
            ShutDownSceneSystem();
            ShutDownInputSystem();
            ShutDownGraphics();

            mEngineSingleton = null;
        }

        // ===== PRIVATE ===== //
        // engine singleton
        static private Engine mEngineSingleton;
        bool mFpsCap = false;
    }
}