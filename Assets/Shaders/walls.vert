#version 330 core
#extension GL_ARB_explicit_uniform_location : require   

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in float texIndex;

layout (location = 0) uniform mat4 uFrustum;
layout (location = 1) uniform mat4 uWorldToCamera;
layout (location = 2) uniform mat4 uLocalToWorld;

out vec2 vTexCoord;
out vec4 vPosition;
flat out int vTexIndex;

void main()
{
    vPosition = uFrustum * uWorldToCamera * uLocalToWorld * vec4(pos, 1.0f);
    vTexCoord = texCoord;
    vTexIndex = int(texIndex);
    gl_Position = vPosition;
}