#version 330 core

#extension GL_ARB_shading_language_420pack : require

vec3 position[] = {vec3(-1.0f, -1.0f, 0.0f),
                   vec3( 1.0f, -1.0f, 0.0f),
                   vec3( 1.0f,  1.0f, 0.0f),
                   vec3( 1.0f,  1.0f, 0.0f),
                   vec3(-1.0f,  1.0f, 0.0f),
                   vec3(-1.0f, -1.0f, 0.0f)};

out vec2 vScreenCoord;

void main()
{
    vScreenCoord = position[gl_VertexID].xy;
    gl_Position = vec4(position[gl_VertexID].xyz, 1.0f);
}