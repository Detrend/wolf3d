#version 330 core
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require   

layout (location = 0) uniform sampler2D uMenuTex;
layout (location = 1) uniform sampler2D uFacesTex;
layout (location = 2) uniform sampler2D uNumbersTex;
layout (location = 3) uniform sampler2D uGunsTex;
layout (location = 4) uniform ivec4 uFaceIndex_Ammo_Score_Floor;
layout (location = 5) uniform ivec4 uLives_Hp_Gun;

in vec2 vTexCoord;

out vec4 result;

const vec2 Face1 = vec2(0.505f, 0.05f);
const vec2 Face2 = vec2(0.595f, 0.95f);

const vec2 Ammo1 = vec2(0.6625f, 0.2f);
const vec2 Ammo2 = vec2(0.734375f, 0.625f);

const vec2 Hp1 = vec2(0.51875f, 0.2f);
const vec2 Hp2 = vec2(0.6f, 0.625f);

const vec2 Lives1 = vec2(0.3375f, 0.2f);
const vec2 Lives2 = vec2(0.4f, 0.625f);

const vec2 Score1 = vec2(0.190625f, 0.2f);
const vec2 Score2 = vec2(0.253125f, 0.625f);

const vec2 Level1 = vec2(0.046875f, 0.2f);
const vec2 Level2 = vec2(0.109375f, 0.625f);

const vec2 Guns1 = vec2(0.778125f, 0.1f);
const vec2 Guns2 = vec2(0.96875f, 0.875f);

vec4 drawRec(vec2 from, vec2 to, sampler2D tex, int indexCount, int index)
{
    if (vTexCoord.x >= from.x && vTexCoord.y >= from.y && vTexCoord.x < to.x && vTexCoord.y < to.y)
    {
        float oneSprite = 1.0f / float(indexCount);
        float texStartX = oneSprite * float(index);
        float texEndX = oneSprite * float(index + 1);

        vec2 spriteCoord = vec2(mix(texStartX, texEndX, (vTexCoord.x - from.x) / (to.x - from.x)), (vTexCoord.y - from.y) / (to.y - from.y));
        vec4 spriteColor = texture(tex, spriteCoord);

        return spriteColor;
    }
    else
    {
        return vec4(0.0f);
    }
}

vec4 drawNumbers(int number, vec2 from, vec2 to)
{
    int num = clamp(number, 0, 99);
    int secondDigit = int(mod(num, 10));
    int firstDigit = (num - secondDigit) / 10;

    vec2 firstDigitStart = from;
    vec2 firstDigitEnd = vec2(mix(from.x, to.x, 0.5f), to.y);
    vec2 secondDigitStart = vec2(firstDigitEnd.x, firstDigitStart.y);
    vec2 secondDigitEnd = to;

    vec4 firstDigitColor = drawRec(firstDigitStart, firstDigitEnd, uNumbersTex, 10, firstDigit);
    vec4 secondDigitColor = drawRec(secondDigitStart, secondDigitEnd, uNumbersTex, 10, secondDigit);

    return firstDigitColor + secondDigitColor;
}

vec4 drawNumber3digit(int number, vec2 from, vec2 to)
{
    int num = clamp(number, 0, 999);
    int thirdDigit = int(mod(num, 10));
    int secondDigit = int(mod(num / 10, 10));
    int firstDigit = int(mod(num / 100, 10));

    vec2 firstDigitStart = from;
    vec2 firstDigitEnd = vec2(mix(from.x, to.x, 0.333f), to.y);
    vec2 secondDigitStart = vec2(firstDigitEnd.x, firstDigitStart.y);
    vec2 secondDigitEnd = vec2(mix(from.x, to.x, 0.666f), to.y);
    vec2 thirdDigitStart = vec2(secondDigitEnd.x, firstDigitStart.y);
    vec2 thirdDigitEnd = to;

    vec4 firstDigitColor = drawRec(firstDigitStart, firstDigitEnd, uNumbersTex, 10, firstDigit);
    vec4 secondDigitColor = drawRec(secondDigitStart, secondDigitEnd, uNumbersTex, 10, secondDigit);
    vec4 thirdDigitColor = drawRec(thirdDigitStart, thirdDigitEnd, uNumbersTex, 10, thirdDigit);

    return firstDigitColor + secondDigitColor + thirdDigitColor;
}

void main()
{
    vec4 color = texture(uMenuTex, vTexCoord);

    // vec4 faceColor = drawRec(Face1, Face2, uFacesTex, 10, uFaceIndex);
    // color = mix(color, faceColor, faceColor.a);
    
    vec4 ammoColor = drawNumbers(uFaceIndex_Ammo_Score_Floor.y, Ammo1, Ammo2);
    color = mix(color, ammoColor, ammoColor.a);

    vec4 hpColor = drawNumber3digit(uLives_Hp_Gun.y, Hp1, Hp2);
    color = mix(color, hpColor, hpColor.a);

    vec4 livesColor = drawNumbers(uLives_Hp_Gun.x, Lives1, Lives2);
    color = mix(color, livesColor, livesColor.a);

    vec4 scoreColor = drawNumber3digit(uFaceIndex_Ammo_Score_Floor.z, Score1, Score2);
    color = mix(color, scoreColor, scoreColor.a);

    vec4 levelColor = drawNumbers(uFaceIndex_Ammo_Score_Floor.w, Level1, Level2);
    color = mix(color, levelColor, levelColor.a);

    vec4 gunsColor = drawRec(Guns1, Guns2, uGunsTex, 4, uLives_Hp_Gun.z);
    color = mix(color, gunsColor, gunsColor.a);

    result = vec4(color.rgb, 1.0f);
}