#version 330 core
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require   

layout (location = 1) uniform sampler2D uGunTexture;
layout (location = 2) uniform int uAnimationIndex;

in vec2 vTexCoord;

out vec4 result;

void main()
{
    vec2 coord = vec2(0.2f * float(uAnimationIndex) + vTexCoord.x / 5, vTexCoord.y);
    vec4 color = texture(uGunTexture, coord);
    if (color.a < 0.1)
        discard;
    result = vec4(color.rgb, 1.0f);
}