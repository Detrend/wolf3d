#version 330 core
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require   

layout (location = 0) uniform vec3 uFloorColor;
layout (location = 1) uniform vec3 uCeilColor;
layout (location = 2) uniform vec4 uHurt_Score;

in vec2 vScreenCoord;

out vec4 result;

const vec4 hurtColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
const vec4 scoreColor = vec4(1.0f, 1.0f, 0.0f, 1.0f);

void main()
{
    vec4 color = vec4(1.0f);
    if (vScreenCoord.y > 0)
    {
        color = vec4(uCeilColor, 1.0);
    }
    else
    {
        color = vec4(uFloorColor, 1.0);
    }

    color = mix(color, hurtColor, uHurt_Score.x);
    color = mix(color, scoreColor, uHurt_Score.y);

    result = vec4(color.rgb, 1.0f);
}