#version 330 core
#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_uniform_location : require   

layout (location = 0) uniform sampler2D uLoadingTexture;

in vec2 vTexCoord;
out vec4 result;

void main()
{
    vec4 color = texture(uLoadingTexture, vTexCoord);

    result = vec4(color.rgb, 1.0f);
}