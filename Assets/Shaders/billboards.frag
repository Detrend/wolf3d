#version 330 core
#extension GL_ARB_explicit_uniform_location : require   

layout (location = 4) uniform sampler2D uTexture;
layout (location = 5) uniform vec4 uHurt_Score;
layout (location = 6) uniform ivec4 uSizeX_SizeY_IndexX_IndexY;

in vec2 vTexCoord;
in vec4 vPosition;

out vec4 result;

const vec4 hurtColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
const vec4 scoreColor = vec4(1.0f, 1.0f, 0.0f, 1.0f);

vec4 pickFromTextureArch(ivec2 count, ivec2 index, vec2 coord)
{
    vec2 spriteSize = vec2(1.0f / count.x, 1.0f / count.y);
    vec2 from = spriteSize * index;
    vec2 to = spriteSize * (index + ivec2(1));
    vec2 spriteCoord = vec2(mix(from.x, to.x, coord.x), mix(from.y, to.y, coord.y));
    vec4 color = texture(uTexture, spriteCoord);
    return color;
}

void main()
{
    vec4 color = pickFromTextureArch(uSizeX_SizeY_IndexX_IndexY.xy, uSizeX_SizeY_IndexX_IndexY.zw, vTexCoord);
    if (color.a < 0.9)
        discard;

    color = mix(color, hurtColor, uHurt_Score.x);
    color = mix(color, scoreColor, uHurt_Score.y);

    result = vec4(color.rgb, 1.0f);
}