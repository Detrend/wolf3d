#version 330 core
#extension GL_ARB_explicit_uniform_location : require   

layout (location = 4) uniform sampler2D uWallTextureS;
layout (location = 5) uniform sampler2D uWallTextureW;
layout (location = 6) uniform sampler2D uWallTextureN;
layout (location = 7) uniform sampler2D uWallTextureE;

layout (location = 8) uniform vec4 uHurt_Score;

in vec2 vTexCoord;
in vec4 vPosition;
flat in int vTexIndex;

out vec4 result;

const vec4 hurtColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
const vec4 scoreColor = vec4(1.0f, 1.0f, 0.0f, 1.0f);

void main()
{
    int Index = int(mod(abs(vTexIndex), 4));

    vec4 color = vec4(0.0f);

    if (Index == 0)
        color = vec4(vec3(texture(uWallTextureS, vTexCoord)), 1.0);
    else if (Index == 1)
        color = vec4(vec3(texture(uWallTextureW, vTexCoord)), 1.0);
    else if (Index == 2)
        color = vec4(vec3(texture(uWallTextureN, vTexCoord)), 1.0);
    else
        color = vec4(vec3(texture(uWallTextureE, vTexCoord)), 1.0);

    color = mix(color, hurtColor, uHurt_Score.x);
    color = mix(color, scoreColor, uHurt_Score.y);

    result = vec4(color.rgb, 1.0f);
}