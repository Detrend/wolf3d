#version 330 core
#extension GL_ARB_explicit_uniform_location : require   

layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 texCoord;

layout (location = 0) uniform mat4 uFrustum;
layout (location = 1) uniform mat4 uWorldToCamera;
layout (location = 2) uniform mat4 uLocalToWorld;

out vec2 vTexCoord;
out vec4 vPosition;

void main()
{
    mat3 antiRotation3 = transpose(mat3(uWorldToCamera));   // transposition of orthonormal matrix = inverse
    mat4 antiRotation = mat4(vec4(antiRotation3[0], 0.0),
                             vec4(antiRotation3[1], 0.0),
                             vec4(antiRotation3[2], 0.0),
                             vec4(0.0, 0.0, 0.0, 1.0));
    vPosition = uFrustum * uWorldToCamera * uLocalToWorld * antiRotation * vec4(pos, 1.0f);
    vTexCoord = texCoord;
    gl_Position = vPosition;
}