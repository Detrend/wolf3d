# Wolf 3D - Remake of Wolfenstein 3D

Wolf 3D is a remake of classic 1992 game “Wolfenstein 3D” in C# using OpenGL.

![alt text](Thumbs/1.png)

This remake contains only the first level of the game; however, Wolf3D is designed
in such a way that expanding it to contain all levels should be as easy as
possible.

![alt text](Thumbs/2.png)
![alt text](Thumbs/3.png)

Created by Matúš Rožek
